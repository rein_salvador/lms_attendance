<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendance extends Model
{
    /**
     * @var string
     */
    protected $table = 'attendances';

    /**
     * @var array 
     */
    protected $fillable = [
        'uuid',
        'user_id',
        'is_late'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_late' => 'boolean'
    ];

    protected $dates = ['created_at'];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function meeting()
    {
        return $this->belongsTo(Meeting::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
