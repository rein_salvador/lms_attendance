<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClassGroup extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];
}
