<?php

namespace App\Exports;

use App\Attendance;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;
use App\User;

class MeetingAttendancesExport implements FromCollection
{

    private $date=null;
    private $userId=null;

    public function __construct($date, $userId)
    {
        $this->date = $date;
        $this->userId = $userId;
    }

    /**
     * Excel header
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'Student name',
            'Time-in',
            'Late',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $date = now()->format('Y-m-d');
        $where = [
            ['created_at', '>=', now()->startOfDay()],
            ['created_at', '<=', now()->endOfDay()],
        ];
        
        if (is_null($this->date) === false) {
            $where = [
                ['created_at', '>=', (new Carbon($this->date))->startOfDay()],
                ['created_at', '<=', (new Carbon($this->date))->endOfDay()],
            ];
            $date = (new Carbon($this->date))->format('Y-m-d');
        }
        
        $data = Attendance::where($where)
            ->with('user')
            ->get()->map(function($attendance) {
                return [
                    $attendance->user->name,
                    $attendance->user->username,
                    $attendance->created_at->format('H:i'),
                    $attendance->is_late ? 'yes' : 'no'
                ];
            })->toArray();

        $data = array_prepend($data, [
            'Student name',
            'Student ID',
            'Time-in',
            'Late',
        ]);

        $data = array_prepend($data, []);
        $data = array_prepend($data, [
            'Date', $date
        ]);
        
        $user = User::find($this->userId);
        if (!! $user) {
            $data = array_prepend($data, []);
            $data = array_prepend($data, [
                'Teacher', $user->username, $user->name
            ]);
        }
 
        return collect($data);
    }

}
