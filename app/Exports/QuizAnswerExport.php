<?php

namespace App\Exports;

use App\Answer;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\User;
use App\Quiz;

class QuizAnswerExport implements FromCollection
{

    private $id;
    private $userId;

    public function __construct($id, $userId=null)
    {
        $this->id = $id;
        $this->userId = $userId;
    }

    /**
     * Excel header
     *
     * @return array
     */
    public function headings(): array
    {
        return [
            'Student name',
            'Score',
        ];
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        $data = Answer::where('quiz_id', $this->id)
            ->with('student', 'quiz')
            ->get()->map(function($answer) {
                return [
                    $answer->student->name,
                    $answer->student->username,
                    $answer->score,
                ];
            })->toArray();

        $data = array_prepend($data, [
            'Student name',
            'Student ID',
            'Score',
        ]);

        $quiz = Quiz::find($this->id);
        $data = array_prepend($data, []);
        $data = array_prepend($data, [
            'Date', $quiz->quiz_date->format('Y-m-d')
        ]);

        $data = array_prepend($data, []);
        $data = array_prepend($data, [
            'Quiz Name', $quiz->title
        ]);
        
        $user = User::find($this->userId);
        if (!! $user) {
            $data = array_prepend($data, []);
            $data = array_prepend($data, [
                'Teacher', $user->username, $user->name
            ]);
        }

        return collect($data);
    }

    /**
    * @var Answer $answer
    */
    public function map($answer): array
    {
        return [
            $answer->student->name,
            $answer->score,
        ];
    }
}
