<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DownloadFileController extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->download(storage_path('app/public/' . $request->file));
    }
}
