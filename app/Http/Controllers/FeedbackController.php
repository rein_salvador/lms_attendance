<?php

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;
use App\Http\Requests\FeedbackRequest;

class FeedbackController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $feedbacks = Feedback::orderBy('created_at', 'desc')
            ->with('student')
            ->where('class_id', $request->get('class_id'))
            ->paginate($request->input('paginate', 50));

        return response()->json([
            'data' => $feedbacks,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FeedbackRequest $request)
    {
        $feedback = Feedback::create([
            'description' => $request->get('description'),
            'student_id' => $request->user()->id,
            'class_id' => $request->user()->class_id,
        ]);

        return response()->json([
            'data' => $feedback
        ], 201);
    }

    /**
     * @param Illuminate/Request $request
     * @param integer $id
     * 
     * @return Illuminate/JsonResponse
     */
    public function destroy(Feedback $feedback)
    {
        $feedback->delete();
        
        return response()->json();
    }
}
