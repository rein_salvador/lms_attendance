<?php

namespace App\Http\Controllers\Student;

use App\Lecture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LectureController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {

        $user = $request->user();
        
        $lecture = Lecture::orderBy('created_at', 'desc')
        ->whereNull('pre_requisite_id')
        ->where('class_id', $user->class_id)
        ->orWhere(function($query) use ($user) {
            $query
                ->whereNotNull('pre_requisite_id')
                ->where('class_id', $user->class_id)
                ->whereHas('preRequisite', function($query) {
                    return $query->whereNotNull('finished_at');
                });
        })
        ->with('preRequisite', 'files')
        ->with([
            'quizzes.answers' => function($query) use ($user) {
                return $query->where('student_id', $user->id);
            }
        ])
        
        // ->doesntHave('answers', function($query) use ($user) {
        // })
        ->paginate($request->input('paginate', 50));

        return response()->json([
            'data' => $lecture
        ]);
    }

    /**
     * Undocumented function
     *
     * @param Lecture $lecture
     * @return void
     */
    public function show(Request $request, $id)
    {
        $user = $request->user();
        $data = Lecture::where('id', $id)
            ->with('files')
            ->with([
                'quizzes.answers' => function($query) use ($user) {
                    return $query->where('student_id', $user->id);
                }
            ])->firstOrFail();

        return response()->json([
            'data' => $data 
        ]);
    }
}
