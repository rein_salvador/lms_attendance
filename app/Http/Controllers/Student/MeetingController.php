<?php

namespace App\Http\Controllers\Student;

use App\Attendance;
use App\Http\Requests\Student\AttendMeeting;
use App\Meeting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class MeetingController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $meeting = Meeting::where([
            ['created_at', '>=', now()->startOfDay()],
            ['created_at', '<=', now()->endOfDay()],
            'class_id' => $request->user()->class_id,
        ])->get()->first();

        $attendance = $request->user()->attendanceToday;
        
        return response()->json([
            'data' => [
                'meeting' => $meeting,
                'attendance' => $attendance, 
            ]
        ], 200);
    }

    /**
     * @param AttendMeeting $request
     * @param Meeting $meeting
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AttendMeeting $request)
    {
        if (is_null($request->user()->attendanceToday) === FALSE) {
            throw new \Exception('Already has attendance today.');
        }
        
        $meeting = Meeting::where([
            ['created_at', '>=', now()->startOfDay()],
            ['created_at', '<=', now()->endOfDay()],
            'class_id' => $request->user()->class_id,
        ])->firstOrFail();
        
        $late = now()->diffInMinutes($meeting->created_at) > 15;

        $attendance = $meeting->attendances()->create([
            'uuid' => $request->uuid,
            'user_id' => $request->user()->id,
            'is_late' => $late,
            'meeting_id' => $meeting->id
        ]);

        return response()->json([
            'data' => $attendance
        ], 201);
    }
}
