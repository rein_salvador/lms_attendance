<?php

namespace App\Http\Controllers\Student;

use App\Http\Requests\Student\AnswerQuiz;
use App\Question;
use App\Quiz;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuizController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $quiz = Quiz::orderBy('created_at', 'desc')
                ->where([
                    'class_id' => $request->user()->class_id,
                ]);
        $user = $request->user();
        
        if ($request->has('lecture_id')) {
            $quiz->where('lecture_id', $request->get('lecture_id'));
        } else {
            $quiz->whereNull('lecture_id');
        }
        
        $data = $quiz->with([
            'answers' => function($query) use ($user) {
                return $query->where('student_id', $user->id);
            }
        ])->paginate($request->input('paginate', 50));

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * @param Quiz $quiz
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = Quiz::where('id', $id)->with('questions')->get();

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * @param Quiz $quiz
     * @param AnswerQuiz $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Quiz $quiz, AnswerQuiz $request)
    {
        $score = 0;
        $answerCheck = $request->user()->answers()->where('quiz_id', $quiz->id)->get()->first();
        
        if (is_null($answerCheck) === FALSE) {
            throw new \Exception('Already answered this quiz.');
        }

        $questions = Question::whereIn('id', array_pluck($request->get('answers'), 'id'))->get();
        
        $answers = $request->get('answers');
        foreach ($questions as $question) {
            $columns = array_pluck($request->get('answers'), 'id');
            $foundKey = array_search($question->id, $columns);

            if ($answers[$foundKey]['answer'] === $question->answer) {
                ++$score;
            }
        }

        $quiz->answers()->create([
            'student_id' => $request->user()->id,
            'answers' => $request->get('answers'),
            'score' => $score
        ]);

        return response()->json([
            'data' => ['score' => $score]
        ]);
    }
}