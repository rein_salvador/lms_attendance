<?php

namespace App\Http\Controllers\Student;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Recitation;
use App\Http\Requests\Student\AnswerRecitation;

class RecitationController extends Controller
{
    public function index(Request $request)
    {
        $recitations = Recitation::orderBy('created_at', 'desc')
                        ->where('class_id', $request->user()->class_id)
                        ->paginate($request->input('paginate', 50));

        return response()->json(['data' => $recitations]);
    }

    public function answer(Recitation $recitation, AnswerRecitation $request)
    {
        $answers = $recitation->answers()->where('student_id', $request->user()->id)->get();
        
        if (empty($answers->toArray()) === FALSE) {
            throw new \Exception('Already answered this reciation.');
        }

        $answer = $recitation->answers()->create([
            'student_id' => $request->user()->id,
            'answer' => $request->get('answer'),
        ]);

        return response()->json([
            'data' => $answer
        ], 201);
    }
}
