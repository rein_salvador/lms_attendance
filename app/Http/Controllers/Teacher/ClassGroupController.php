<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ClassGroup;

class ClassGroupController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $classGroups = ClassGroup::paginate($request->input('paginate', 50)); 

        return response()->json([
            'data' => $classGroups
        ]);
    }
}
