<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Requests\Teacher\AddLecture;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Lecture;

class LectureController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $lectures = $request->user()
            ->lectures()
                ->with('preRequisite', 'quizzes', 'files')
                ->orderBy('created_at', 'desc')
                ->where('class_id', $request->get('class_id'))
            ->paginate($request->input('paginate', 50)); 

        return response()->json([
            'data' => $lectures
        ]);
    }

    /**
     * @param AddLecture $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddLecture $request)
    {
        $user = $request->user();
        \DB::beginTransaction();
        try {

            $lecture = $user->lectures()->create([
                'title' => $request->get('title'),
                'passing_score' => $request->get('passing_score'),
                'description' => $request->get('description'),
                'pre_requisite_id' => $request->get('pre_requisite_id'),
                'class_id' => $request->get('class_id'),
                'filename' => 'a'
            ]);
            
            $files = [];
            foreach ($request->file('files') as $key => $file) {
                $fullFilename = $file->getClientOriginalName();
                $newFilename =  pathinfo($fullFilename)['filename'] . '_' . time() . '.' . $file->getClientOriginalExtension(); 
                
                $path = $file->storeAs('lectures', $newFilename, 'public');
    
                $files[] = $lecture->files()->create([
                    'filename' => pathinfo($fullFilename)['filename'] . '.' . $file->getClientOriginalExtension(),
                    'filepath' => $path,
                    'lecture_id' => $lecture->id
                ]);
            }
            
            $lecture['files'] = $files;

            \DB::commit();
        } catch (\Exception $e) {
            \DB::rollback();
        }

        return response()->json([
            'data'  => $lecture
        ], 201);
    }

    /**
     * Tag lecture as finish
     *
     * @param Lecture $lecture
     * @param Integer $id
     * @return void
     */
    public function finish(Lecture $lecture)
    {
        $lecture->finished_at = \Carbon\Carbon::now();
        $lecture->save();

        return response()->json([
            'data' => $lecture
        ]);
    }
}
