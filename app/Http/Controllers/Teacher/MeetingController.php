<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Requests\Teacher\AddMeeting;
use App\Meeting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\MeetingAttendancesExport;
use Maatwebsite\Excel\Facades\Excel;

class MeetingController extends Controller
{

    /**
     * @param Request $request
     */
    public function index(Request $request)
    {
        $data = $request->user()->meetings()
        ->orderBy('created_at', 'desc')
        ->where('class_id', $request->get('class_id'))
        ->get();

        return response()->json(['data' => $data]);
    }

    /**
     * @param AddMeeting $meetingRequest
     */
    public function store(AddMeeting $meetingRequest)
    {
        $user = $meetingRequest->user();

        $data = $user->meetings()->updateOrCreate([
            ['created_at', '>=', now()->startOfDay()],
            ['created_at', '<=', now()->endOfDay()],
        ],
        [
            'code' => $meetingRequest->get('code'),
            'class_id' => $meetingRequest->get('class_id'),
        ]);

        return response()->json(['data' => $data], 201);
    }

    /**
     * @param Illuminate/Request $request
     * @param integer $id
     * 
     * @return Illuminate/JsonResponse
     */
    public function show(Meeting $meeting)
    {
        $data = Meeting::where('id', $meeting->id)
        ->with('attendances')->get()->first();

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * @param Request $request
     */
    public function today(Request $request)
    {
        $data = $request->user()->meetings()
        ->with('attendances')
        ->where([
            ['created_at', '>=', now()->startOfDay()],
            ['created_at', '<=', now()->endOfDay()],
            'class_id' => $request->get('class_id'),
        ])->get()->first();

        return response()->json(['data' => $data]);
    }

    /**
     * @param Illuminate/Request $request
     * @param integer $id
     * 
     * @return Illuminate/JsonResponse
     */
    public function destroy(Meeting $meeting)
    {
        $meeting->attendances()->delete();
        $meeting->delete();
        
        return response()->json();
    }

    /**
     * Download quiz and scores in excel format
     *
     * @param Integer $id
     * @return void
     */
    public function download(Request $request)
    {
        return Excel::download(new MeetingAttendancesExport($request->date, $request->teacher_id), 'meeting_report.xlsx');
    }
}
