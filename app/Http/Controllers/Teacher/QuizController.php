<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Requests\Teacher\AddQuiz;
use App\Quiz;
use App\Answer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exports\QuizAnswerExport;
use Maatwebsite\Excel\Facades\Excel;

class QuizController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $quizzes = $request->user()
            ->quizzes()
                ->orderBy('created_at', 'desc')
                ->where('class_id', $request->get('class_id'))
            ->paginate($request->input('paginate', 50));

        return response()->json([
            'data' => $quizzes,
        ]);
    }

    /**
     * @param Quiz $quiz
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $data = Quiz::where('id', $id)->with('questions')->get();

        return response()->json([
            'data' => $data
        ]);
    }

    /**
     * @param AddQuiz $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AddQuiz $request)
    {
        $user = $request->user();

        $quiz = $user->quizzes()->create([
            'title' => $request->get('title'),
            'time_limit' => $request->get('time_limit'),
            'lecture_id' => $request->get('lecture_id'),
            'class_id' => $request->get('class_id'),
            'quiz_date' => now()
        ]);

        foreach($request->get('questions') as $question) {
            $quiz->questions()->create([
                'question' => $question['question'],
                'answer' => $question['answer'],
                'choices' => $question['choices']
            ]);
        }

        return response()->json([
            'data' => $quiz,
        ], 201);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function scores($id)
    {
        Quiz::findOrFail($id);

        $data = Answer::where('quiz_id', $id)
            ->with('student', 'quiz')
            ->get();

        return response()->json([
            'data' => $data, 
        ]);
    }

    /**
     * Download quiz and scores in excel format
     *
     * @param Integer $id
     * @return void
     */
    public function download(Request $request, $id)
    {
        return Excel::download(new QuizAnswerExport($id, $request->teacher_id), 'quiz_report.xlsx');
    }
}
