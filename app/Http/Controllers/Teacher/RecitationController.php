<?php

namespace App\Http\Controllers\Teacher;

use Illuminate\Http\Request;
use App\Recitation;
use App\Http\Controllers\Controller;
use App\Http\Requests\Teacher\RecitationResource;

class RecitationController extends Controller
{
    
    public function index(RecitationResource $request)
    {
        $recitations = Recitation::orderBy('created_at', 'desc')
            ->where('class_id', $request->get('class_id'))
            ->paginate($request->input('paginate', 50));

        return response()->json([
            'data' => $recitations
        ]);
    }

    public function store(RecitationResource $request)
    {
        $recitation = Recitation::create([
            'question' => $request->get('question'),
            'teacher_id' => $request->user()->id,
            'class_id' => $request->get('class_id'),
        ]);

        return response()->json([
            'data' => $recitation
        ], 201);
    }

    public function show(RecitationResource $request, $id)
    {
        return response()->json([
            'data' => Recitation::where('id', $id)->with('answers.student')->get()->first()
        ]);
    }

    public function destroy(Recitation $recitation, RecitationResource $request)
    {
        $recitation->answers()->delete();
        $recitation->delete();

        return response()->json();
    }
}
