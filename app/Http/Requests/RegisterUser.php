<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // 'name' => 'required',
            'username' => 'required|max:30|unique:users',
            'name' => 'required|string|max:255',
            'password' => 'required',
            'user_type' => 'required|in:student,teacher',
            // 'password' => 'required|string|min:6|confirmed',
            // 'email' => 'required|email|unique:users,email',
            // 'email' => 'required|string|email|max:255|unique:users',
        ];
    }
}
