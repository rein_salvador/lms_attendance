<?php

namespace App\Http\Requests\Student;

use Illuminate\Foundation\Http\FormRequest;
use App\Meeting;
use App\Attendance;

class AttendMeeting extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $where = [
            ['created_at', '>=', now()->startOfDay()],
            ['created_at', '<=', now()->endOfDay()],
        ];

        $meeting = Meeting::where($where)->firstOrFail();

        return [
            'code' => [
                'required', 
                function ($attribute, $value, $fail) use ($meeting) {
                    if ($value !== $meeting->code) {
                        return $fail($attribute . ' is invalid.');
                    }
                }
            ],
            'uuid' => [
                'required', 
                function ($attribute, $value, $fail) use ($where) {
                    $where['uuid'] = $value;
                    $attendance = Attendance::where($where)->first();

                    if ($attendance) {
                        return $fail($attribute . ' has already been used today.');
                    }
                }
            ]
        ];
    }
}
