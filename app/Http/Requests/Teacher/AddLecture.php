<?php

namespace App\Http\Requests\Teacher;

use Illuminate\Foundation\Http\FormRequest;

class AddLecture extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'files' => 'required|array',
            'files.*' => 'file',
            'title' => 'required',
            'passing_score' => 'numeric|min:0',
            'description' => 'required',
            'pre_requisite_id' => 'nullable|exists:lectures,id',
        ];
    }
}
