<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Lecture extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'title',
        'description',
        'filename',
        'passing_score',
        'pre_requisite_id',
        'finished_at',
        'class_id',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'created_at'
    ];

    /**
     * @return string
     */
    public function getFilenameAttribute($value)
    {
        return config('app.url') . Storage::url($value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function teacher()
    {
        return $this->belongsTo(User::class, 'teacher_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function answers()
    {
        return $this->hasManyThrough(Answer::class, Quiz::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quizzes()
    {
        return $this->hasMany(Quiz::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany(LectureFile::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function preRequisite()
    {
        return $this->belongsTo(Lecture::class, 'pre_requisite_id');
    }
}
