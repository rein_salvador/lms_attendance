<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class LectureFile extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'filepath',
        'filename',
        'lecture_id',
    ];

    protected $appends = [
        'downloadpath'
    ];

    /**
     * @return string
     */
    public function getFilepathAttribute($value)
    {
        return config('app.url') . Storage::url($value);
    }

    /**
     * @return string
     */
    public function getDownloadpathAttribute()
    {
        return config('app.url') . '/file/download?file=' . $this->getOriginal('filepath');
    }
}
