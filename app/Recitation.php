<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recitation extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'teacher_id',
        'question',
        'class_id',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(RecitationAnswer::class)->with('student');
    }
}
