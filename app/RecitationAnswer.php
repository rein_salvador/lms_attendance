<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecitationAnswer extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'answer',
        'student_id',
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'updated_at'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function student()
    {
        return $this->belongsTo(User::class, 'student_id', 'id');
    }
}


