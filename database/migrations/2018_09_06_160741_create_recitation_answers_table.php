<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecitationAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recitation_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->text('answer');
            $table->integer('student_id')->nullable()->index()->unsigned();
            $table->integer('recitation_id')->nullable()->index()->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recitation_answers');
    }
}
