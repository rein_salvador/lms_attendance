@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">Classes</div>

                <div class="card-body">
                <form method="POST" action="{{ route('classes.update', $class) }}">
                    @csrf
                    {{ method_field('PATCH') }}
                
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input type="text" value="{{ old('name') ?? $class->name }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name">

                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="form-group row mb-0">
                        <div class="col-md-6 offset-md-4 d-flex justify-content-end">
                            <a href="{{ route('classes.index') }}" class="btn btn-default">
                                {{ __('Back') }}
                            </a>
                            <button type="submit" class="btn btn-primary ml-2">
                                {{ __('Submit') }}
                            </button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection