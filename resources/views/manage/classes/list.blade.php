@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header d-flex justify-content-between">
                    <span>Classes</span>
                    <a href="{{route('classes.create')}}" class="btn btn-success">Create class</a>
                </div>

                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success" role="alert">
                            {{ Session::get('message') }}
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Created at</th>
                            <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($classes as $class)
                                <tr>
                                    <th scope="row">{{ $class->id }}</th>
                                    <td>{{ $class->name }}</td>
                                    <td>{{ $class->created_at }}</td>
                                    <td>
                                        <a href="{{ route('classes.edit', $class) }}" class="btn btn-icons btn-primary">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection