<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');

Route::group(['middleware' => 'auth:api'], function() {
    Route::get('me', function(Request $request) {
        return $request->user();
    });

    Route::resource('feedbacks', 'FeedbackController', ['only' => ['index', 'store', 'destroy']]);

    //teacher stuff
    Route::group(['prefix' => 'teacher', 'namespace' => 'Teacher'], function() {
        Route::get('lectures', 'LectureController@index');
        Route::get('classes', 'ClassGroupController@index');
        Route::post('lectures', 'LectureController@store');
        Route::patch('lectures/{lecture}/finish', 'LectureController@finish');
        Route::get('quizzes', 'QuizController@index');
        Route::get('quizzes/{quiz}', 'QuizController@show');
        Route::post('quizzes', 'QuizController@store');
        Route::get('quizzes/{quiz}/scores', 'QuizController@scores');
        Route::get('meetings/today', 'MeetingController@today');
        Route::resource('meetings', 'MeetingController', ['only' => ['index', 'store', 'show', 'destroy']]);
        Route::resource('recitations', 'RecitationController', ['only' => ['index', 'store', 'show', 'destroy']]);
    });

    //student stuff
    Route::group(['prefix' => 'student', 'namespace' => 'Student'], function() {
        Route::get('lectures', 'LectureController@index');
        Route::get('lectures/{lecture}', 'LectureController@show');
        Route::resource('quizzes', 'QuizController', ['only' => ['index', 'show', 'update']]);
        Route::get('attendances', 'AttendanceController@index');
        Route::get('meetings/today', 'MeetingController@index');
        Route::post('meetings', 'MeetingController@store');
        Route::post('recitations/{recitation}/answer', 'RecitationController@answer');
        Route::get('recitations', 'RecitationController@index');
    });
});