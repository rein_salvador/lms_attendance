<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function() {
    Route::group(['prefix' => 'manage', 'namespace' => 'Web'], function() {
        Route::resource('students', 'StudentController');
        Route::resource('classes', 'ClassesController');
    });
});

Route::group(['prefix' => 'reports', 'namespace' => 'Teacher'], function() {
    Route::get('quiz/{id}/download', 'QuizController@download');
    Route::get('meeting/download', 'MeetingController@download');
});

Route::get('file/download', 'DownloadFileController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
