# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.23)
# Database: lms
# Generation Time: 2019-01-11 02:31:14 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table answers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `answers`;

CREATE TABLE `answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quiz_id` int(10) unsigned NOT NULL,
  `student_id` int(10) unsigned NOT NULL,
  `answers` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `score` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `answers_quiz_id_index` (`quiz_id`),
  KEY `answers_student_id_index` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;

INSERT INTO `answers` (`id`, `quiz_id`, `student_id`, `answers`, `score`, `created_at`, `updated_at`)
VALUES
	(6,3,2,'[{\"id\":\"4\",\"answer\":\"a\"},{\"id\":\"5\",\"answer\":\"b\"}]',2,'2018-08-30 00:46:56','2018-08-30 00:46:56'),
	(7,8,3,'[{\"id\":\"14\",\"answer\":\"b\"},{\"id\":\"15\",\"answer\":\"c\"}]',0,'2018-12-12 14:52:46','2018-12-12 14:52:46'),
	(12,8,2,'[{\"id\":\"14\",\"answer\":\"a\"},{\"id\":\"15\",\"answer\":\"a\"}]',1,'2018-12-12 16:14:16','2018-12-12 16:14:16');

/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table attendances
# ------------------------------------------------------------

DROP TABLE IF EXISTS `attendances`;

CREATE TABLE `attendances` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meeting_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `is_late` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attendances_meeting_id_index` (`meeting_id`),
  KEY `attendances_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `attendances` WRITE;
/*!40000 ALTER TABLE `attendances` DISABLE KEYS */;

INSERT INTO `attendances` (`id`, `uuid`, `meeting_id`, `user_id`, `is_late`, `created_at`, `updated_at`)
VALUES
	(7,'myuuid',26,2,0,'2018-08-31 00:06:42','2018-08-31 00:06:42'),
	(8,'myuuid',27,3,0,'2018-09-03 21:16:31','2018-09-03 21:16:31'),
	(9,'myuuid',28,3,0,'2018-09-04 23:45:45','2018-09-04 23:45:45'),
	(10,'sample',29,2,0,'2018-11-27 22:23:18','2018-11-27 22:23:18'),
	(11,'uuid',30,2,0,'2018-11-28 22:26:52','2018-11-28 22:26:52'),
	(13,'uuid',30,3,1,'2018-11-28 22:44:57','2018-11-28 22:44:57'),
	(14,'uuid',31,2,0,'2018-12-11 13:04:15','2018-12-11 13:04:15'),
	(15,'uuid',32,2,0,'2019-01-07 23:36:29','2019-01-07 23:36:29'),
	(16,'uuid',32,3,0,'2019-01-07 23:36:46','2019-01-07 23:36:46'),
	(17,'uuid',33,2,0,'2019-01-08 16:01:53','2019-01-08 16:01:53');

/*!40000 ALTER TABLE `attendances` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table feedbacks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `feedbacks`;

CREATE TABLE `feedbacks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `student_id` int(10) unsigned DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `feedbacks_student_id_index` (`student_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `feedbacks` WRITE;
/*!40000 ALTER TABLE `feedbacks` DISABLE KEYS */;

INSERT INTO `feedbacks` (`id`, `student_id`, `description`, `created_at`, `updated_at`, `deleted_at`)
VALUES
	(1,2,'Are u sure?','2018-09-05 00:53:53','2019-01-08 22:32:42','2019-01-08 22:32:42'),
	(2,2,'Are u sure?','2018-09-05 00:56:14','2019-01-08 22:33:11','2019-01-08 22:33:11'),
	(3,2,'What happens if thisss','2018-12-02 15:21:46','2018-12-02 15:21:46',NULL),
	(4,2,'Hftgv','2018-12-02 15:22:57','2018-12-02 15:22:57',NULL),
	(5,2,'Is that what is really looks like?','2018-12-11 13:06:23','2018-12-11 13:06:23',NULL),
	(6,2,'Hey there?','2019-01-06 01:57:58','2019-01-06 01:57:58',NULL),
	(7,2,'Hey again?','2019-01-06 10:05:32','2019-01-06 10:05:32',NULL);

/*!40000 ALTER TABLE `feedbacks` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table lecture_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lecture_files`;

CREATE TABLE `lecture_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lecture_id` int(10) unsigned DEFAULT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `filepath` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lecture_files_lecture_id_index` (`lecture_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `lecture_files` WRITE;
/*!40000 ALTER TABLE `lecture_files` DISABLE KEYS */;

INSERT INTO `lecture_files` (`id`, `lecture_id`, `filename`, `filepath`, `created_at`, `updated_at`)
VALUES
	(1,31,NULL,NULL,'2019-01-09 22:25:24','2019-01-09 22:25:24'),
	(2,32,NULL,NULL,'2019-01-09 22:25:32','2019-01-09 22:25:32'),
	(3,33,NULL,NULL,'2019-01-09 22:25:59','2019-01-09 22:25:59'),
	(4,34,NULL,NULL,'2019-01-09 22:26:39','2019-01-09 22:26:39'),
	(5,35,NULL,NULL,'2019-01-09 22:28:52','2019-01-09 22:28:52'),
	(6,36,NULL,NULL,'2019-01-09 22:31:32','2019-01-09 22:31:32'),
	(9,38,'HRM-HAN-001 Employee Handbook TAS Group Philippines.pdf','lectures/HRM-HAN-001 Employee Handbook TAS Group Philippines_1547044502.pdf','2019-01-09 22:35:02','2019-01-09 22:35:02'),
	(10,38,'HRM-HAN-001 Employee Handbook TAS Group Philippines.pdf','lectures/HRM-HAN-001 Employee Handbook TAS Group Philippines_1547044502.pdf','2019-01-09 22:35:02','2019-01-09 22:35:02'),
	(11,39,'quiz_report (2).xlsx','lectures/quiz_report (2)_1547046586.xlsx','2019-01-09 23:09:46','2019-01-09 23:09:46'),
	(12,39,'quiz_report (1).xlsx','lectures/quiz_report (1)_1547046586.xlsx','2019-01-09 23:09:46','2019-01-09 23:09:46'),
	(13,40,'quiz_report (2).xlsx','lectures/quiz_report (2)_1547047837.xlsx','2019-01-09 23:30:37','2019-01-09 23:30:37'),
	(14,40,'JonelynViloria.pdf','lectures/JonelynViloria_1547047838.pdf','2019-01-09 23:30:38','2019-01-09 23:30:38');

/*!40000 ALTER TABLE `lecture_files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table lectures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `lectures`;

CREATE TABLE `lectures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `passing_score` int(11) NOT NULL DEFAULT '0',
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `pre_requisite_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lectures_teacher_id_index` (`teacher_id`),
  KEY `lectures_pre_requisite_id_index` (`pre_requisite_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `lectures` WRITE;
/*!40000 ALTER TABLE `lectures` DISABLE KEYS */;

INSERT INTO `lectures` (`id`, `teacher_id`, `title`, `passing_score`, `description`, `filename`, `created_at`, `updated_at`, `pre_requisite_id`)
VALUES
	(5,1,'oopps',0,'heyyyyee','lectures/L2Tl70b1vtsV6jtSjNeiLCBvQQX3liRhfM64NqyH.docx','2018-08-29 23:15:21','2018-08-29 23:15:21',NULL),
	(6,1,'Sample 5',0,'heyyyyee','lectures/examfile_1535901068.docx','2018-09-02 23:11:08','2018-09-02 23:11:08',NULL),
	(7,1,'Sample 5',4,'heyyyyee','lectures/examfile_1535901274.docx','2018-09-02 23:14:34','2018-09-02 23:14:34',5),
	(8,1,'Sample 5',3,'heyyyyee','lectures/examfile_1535907311.docx','2018-09-03 00:55:11','2018-09-03 00:55:11',5),
	(9,1,'Ggghh',4,'Hgdcf','lectures/Probationary Employment Agreement Reniel_1538759395.pdf','2018-10-06 01:09:55','2018-10-06 01:09:55',NULL),
	(10,1,'Sample',4,'123123','lectures/JonelynViloria_1544451324.pdf','2018-12-10 14:15:24','2018-12-10 14:15:24',NULL),
	(11,1,'Evolution of man',3,'This contains the evolution of man. The secret that should not be revealed and must be buried forever.','lectures/JonelynViloria_1544628346.pdf','2018-12-12 15:25:46','2018-12-12 15:25:46',NULL),
	(12,1,'Lesson Sample',4,'This is a sample lesso  that we need to discuss in class in order to promote environmental awareness.','lectures/Functional-Requirements-App-v0.1_1546958943.pdf','2019-01-08 22:49:03','2019-01-08 22:49:03',NULL),
	(36,1,'Life of rizal',10,'This has been one of the most popular book in the whole world','a','2019-01-09 22:31:32','2019-01-09 22:31:32',NULL),
	(38,1,'Life of rizal prt 2',10,'This has been one of the most popular book in the whole world','a','2019-01-09 22:35:02','2019-01-09 22:35:02',NULL),
	(39,1,'Life of pi Vol. 1',10,'The story of an indian man and his companion tiger','a','2019-01-09 23:09:46','2019-01-09 23:09:46',NULL),
	(40,1,'Bookish',3,'asdfwef','a','2019-01-09 23:30:37','2019-01-09 23:30:37',NULL);

/*!40000 ALTER TABLE `lectures` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table meetings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `meetings`;

CREATE TABLE `meetings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `teacher_id` int(10) unsigned NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `meetings_teacher_id_index` (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `meetings` WRITE;
/*!40000 ALTER TABLE `meetings` DISABLE KEYS */;

INSERT INTO `meetings` (`id`, `teacher_id`, `code`, `created_at`, `updated_at`)
VALUES
	(1,1,'test','2018-08-28 18:52:18','2018-08-28 18:52:18'),
	(26,1,'12345','2018-08-31 00:02:21','2018-08-31 00:02:21'),
	(27,1,'12345','2018-09-03 21:16:21','2018-09-03 21:16:21'),
	(28,1,'12345','2018-09-04 23:45:29','2018-09-04 23:45:29'),
	(29,1,'12345','2018-11-27 22:16:36','2018-11-27 22:16:36'),
	(30,1,'123456','2018-11-28 22:20:40','2018-11-28 22:20:40'),
	(31,1,'12345','2018-12-11 13:03:35','2018-12-11 13:03:35'),
	(32,1,'123456','2019-01-07 23:36:07','2019-01-07 23:36:07'),
	(33,1,'54321','2019-01-08 15:54:57','2019-01-08 15:54:57');

/*!40000 ALTER TABLE `meetings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(1,'2014_10_12_000000_create_users_table',1),
	(2,'2014_10_12_100000_create_password_resets_table',1),
	(3,'2016_06_01_000001_create_oauth_auth_codes_table',1),
	(4,'2016_06_01_000002_create_oauth_access_tokens_table',1),
	(5,'2016_06_01_000003_create_oauth_refresh_tokens_table',1),
	(6,'2016_06_01_000004_create_oauth_clients_table',1),
	(7,'2016_06_01_000005_create_oauth_personal_access_clients_table',1),
	(8,'2018_08_08_135003_create_meetings_table',1),
	(9,'2018_08_08_135650_create_lectures_table',1),
	(10,'2018_08_08_144842_create_quizzes_table',1),
	(11,'2018_08_08_152129_create_questions_table',1),
	(12,'2018_08_08_155618_create_answers_table',1),
	(13,'2018_08_10_022617_create_attendances_table',1),
	(14,'2018_08_10_022908_insert_user_id_on_attendances_table',1),
	(16,'2018_08_30_153634_add_uui_column_to_attendances_table',2),
	(17,'2018_09_02_162846_add_columns_to_lectures_table',3),
	(18,'2018_09_02_171513_add_column_to_quizzes_table',4),
	(20,'2018_09_04_163314_create_feedback_table',5),
	(23,'2018_09_06_155713_create_recitations_table',6),
	(24,'2018_09_06_160741_create_recitation_answers_table',6),
	(29,'2019_01_08_145707_add_username_column_in_users_table',7),
	(31,'2019_01_08_222405_add_soft_delete_to_feedbacks_table',8),
	(33,'2019_01_09_214936_create_lecture_files_table',9);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table oauth_access_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_access_tokens`;

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`)
VALUES
	('0059122c2f1ead28b23ad9c7cc150edd26ac3c38961f54415b541ab2017f4070ba3d078d09196556',1,2,NULL,'[]',0,'2019-01-08 21:49:47','2019-01-08 21:49:47','2020-01-08 21:49:47'),
	('021e408daa908c49af8a2bd20b7ecff14d6301c13f1b2c0aec6ae09fcdb0e6ada2ed2bd423e6ab0d',2,2,NULL,'[]',0,'2018-11-28 22:20:57','2018-11-28 22:20:57','2019-11-28 14:20:57'),
	('040524ddd4409cfd752730205c137b2cd4a4a4efb58b81e3e189ccd392565a697131a46ce98cd2ab',20,2,NULL,'[]',0,'2018-12-02 16:26:25','2018-12-02 16:26:25','2019-12-02 08:26:25'),
	('053175bd6a0868481f8207ca5799ad9cbfcf24e86083a5a3156f151a353d61cfbfda03b4a956b453',18,2,NULL,'[]',0,'2018-11-18 23:08:00','2018-11-18 23:08:00','2019-11-18 15:08:00'),
	('05531b0bab6a5627a83ff4a99c1564589ae63174d16f0b36bb884d16952cb247cab43f86820dd140',2,2,NULL,'[]',0,'2019-01-06 01:57:51','2019-01-06 01:57:51','2020-01-06 01:57:51'),
	('0715b2ddb482aba2c94a34ae05a9376edcfad9e4ce19a3e7debdfd0f9f774ef0297bfdf65eaa56bb',1,2,NULL,'[]',0,'2019-01-06 01:39:12','2019-01-06 01:39:12','2020-01-06 01:39:12'),
	('0942e0c7d68d4e7f6ab4567472681c63f8cc8e3f5b289376766097ec0977391c7210cbecfba93007',1,2,NULL,'[]',0,'2018-11-27 21:46:17','2018-11-27 21:46:17','2019-11-27 13:46:17'),
	('0ac9f76173717bd3388d3a3d7276f06919424bf79abbeca232fe3da759baa33b7ad7152a73a2f03d',1,2,NULL,'[]',0,'2018-12-10 13:44:24','2018-12-10 13:44:24','2019-12-10 13:44:24'),
	('0c0b7380162f42c83f9644c18f53c441e09b61f9680d53427c8e26a635238acc57f80d7180de4b41',16,2,NULL,'[]',0,'2018-11-18 23:07:17','2018-11-18 23:07:17','2019-11-18 15:07:17'),
	('0dfd45786739a427439ff49f8eab0edaca5d59b2d0e5a72ae4412c654c0dcf07e5c2a15c2ea160a2',16,2,NULL,'[]',0,'2018-11-18 22:52:56','2018-11-18 22:52:56','2019-11-18 14:52:56'),
	('0e35b43efb7e0d0a2f71f30e00990cc3d7cd3474abab65cb224c71ecf4b85cef8d2e52b15c39ba0d',1,2,NULL,'[]',0,'2019-01-11 10:24:44','2019-01-11 10:24:44','2020-01-11 10:24:44'),
	('14e0fc21e8f914c5202ac954413a5ed12748e8254a5ee2ff9e5905a87ee52af5e8894fc324fc2002',2,2,NULL,'[]',0,'2018-12-27 05:38:02','2018-12-27 05:38:02','2019-12-27 05:38:02'),
	('15c83d83439c6a9f3dcf36126d4ac52d17884282a5925697fb89770cf800c4fbb348ebc0b37f2403',1,2,NULL,'[]',0,'2019-01-11 10:15:53','2019-01-11 10:15:53','2020-01-11 10:15:53'),
	('198ca028df7b9a6d8d2999edf9e609fe1a86854da027364f642eba6c4472ccb020bcaeb72121cf66',3,2,NULL,'[]',0,'2019-01-08 16:02:09','2019-01-08 16:02:09','2020-01-08 16:02:09'),
	('19a1bca9a0012b5a2ad079585e1bf626f9e66d0ab2976c89fe16ed8e0582aa3fe3c1ad7d2a5457ee',2,2,NULL,'[]',0,'2018-12-27 05:32:08','2018-12-27 05:32:08','2019-12-27 05:32:08'),
	('1db4d6b31033572faf18904d69d9d52b0a7f8217a5aac677fec685fe42275a0cb596dfa794bc358e',1,2,NULL,'[]',0,'2019-01-08 15:57:48','2019-01-08 15:57:48','2020-01-08 15:57:48'),
	('1f56bc943a7985e97a1e10fc3bd6d399bed1f270578f88bf60681dcf7b2b5715ba2948e34c66aab2',2,2,NULL,'[]',0,'2018-11-28 22:24:19','2018-11-28 22:24:19','2019-11-28 14:24:19'),
	('226a89b44d5dcaa42fa49c53ff1b6305097d7afbfd11e9367f4e1d01eeb2502ebf3faa4811a012de',16,2,NULL,'[]',0,'2018-11-18 22:57:25','2018-11-18 22:57:25','2019-11-18 14:57:25'),
	('25f83c578ac96d850d1622b97cd3f831d2e92e2031eb9e16eec50b2af5a78d63da4d16442bc62c71',1,2,NULL,'[]',0,'2019-01-07 23:17:04','2019-01-07 23:17:04','2020-01-07 23:17:04'),
	('2686917af67f0c6dceae7a3d0858a61d16d0bc4263b7abd7fe7b99cd0a31437d7039d3a76481bd86',1,2,NULL,'[]',0,'2019-01-07 22:22:19','2019-01-07 22:22:19','2020-01-07 22:22:19'),
	('29cf3bcb984b867f20865e267dd7432608c69f9ee7e7b951d3f7b58c6c2f61a327b254bb8482c810',1,2,NULL,'[]',0,'2018-12-15 10:02:48','2018-12-15 10:02:48','2019-12-15 10:02:48'),
	('2ea4b3d9e24566a40b2279aea121c37d20783cbd0a0c75a06bce116e36e61ef45b883f0d18063058',2,2,NULL,'[]',0,'2018-12-02 16:18:39','2018-12-02 16:18:39','2019-12-02 08:18:39'),
	('328ccd54acd0101572fda19e66d5ca2bdb4f86b40f45fa2ee7c021ccb52fdcca44c6989a014dd60b',13,2,NULL,'[]',0,'2018-11-18 18:23:28','2018-11-18 18:23:28','2019-11-18 10:23:28'),
	('347a79ed57aa8fda0674dfad466f9427419bc4840a0aa09c0007493cf072dcbdb7646c6ae932b9b1',1,2,NULL,'[]',0,'2018-09-04 23:42:21','2018-09-04 23:42:21','2019-09-04 15:42:21'),
	('349f767429274d8be3c9d1f34ebd207fc59ff5a53a36793db5b90ec23477bd456f7459ec5a44ad6e',2,2,NULL,'[]',0,'2018-12-11 13:03:54','2018-12-11 13:03:54','2019-12-11 13:03:54'),
	('351050b2b40c693a1ee76b2be4a7103af6487093a6c7715f3af8769033ab43846a00e6d2c3c160b3',1,2,NULL,'[]',0,'2018-10-21 17:48:14','2018-10-21 17:48:14','2019-10-21 09:48:14'),
	('3553d89c6b93ee57b33c6de1242afa4ef74addf6863b9241e2b0c5b2d76be855ca4a694cdc0fdaea',2,2,NULL,'[]',0,'2018-12-12 16:07:52','2018-12-12 16:07:52','2019-12-12 16:07:52'),
	('37ce11011f95866d6acd8380eeb73b38e1ce3f86496c4a7b7541240bf78d87e16dbb5342e13c0b31',14,2,NULL,'[]',0,'2018-11-18 18:31:05','2018-11-18 18:31:05','2019-11-18 10:31:05'),
	('38327a67d1db463e55d95ce6b21dc8132339c01b902ea9f728834277aa861bc8c12437d870d0c1af',1,2,NULL,'[]',0,'2018-10-21 11:13:32','2018-10-21 11:13:32','2019-10-21 03:13:32'),
	('39e5dcd8314a2d30f7e26c3e5fecab188dd94fe314b8b9de5d1c34025da11ea521fc08452f2ef1d2',1,2,NULL,'[]',0,'2018-12-11 13:06:38','2018-12-11 13:06:38','2019-12-11 13:06:38'),
	('3cb4be1da47fcabfbf6807097a07ca2c09d299a8b326cec664acac2a009c9e93fc8ca702c853e795',2,2,NULL,'[]',0,'2018-11-27 22:40:15','2018-11-27 22:40:15','2019-11-27 14:40:15'),
	('3e8895402d602bc037326ac12f37eaf71809fe492c35708fa218202937ed8f92ed62a7d87702d637',1,2,NULL,'[]',0,'2018-11-27 21:59:18','2018-11-27 21:59:18','2019-11-27 13:59:18'),
	('450483275c6a5d4200841bf1a637e0193a3d1b6b5d8ee6977ec6591768597976f799642d6376b503',1,2,NULL,'[]',0,'2018-11-27 22:46:06','2018-11-27 22:46:06','2019-11-27 14:46:06'),
	('49f12d60ca97f5d1a0de7d407166dc35852ca63260c4f491b4804a26bd615408cff16f26eb2c550c',16,2,NULL,'[]',0,'2018-11-18 23:47:41','2018-11-18 23:47:41','2019-11-18 15:47:41'),
	('4acfa67e61d2a1e6bffd4442f39211dbce0a328cd5e0c5fe42d6f5859b3b96d50097f208718dec7e',1,2,NULL,'[]',0,'2019-01-09 21:26:49','2019-01-09 21:26:49','2020-01-09 21:26:49'),
	('52d9a9f09462e911c3d9b1a4f8260685644d6d7c16d6655fa802bced55034cf82d72744566696d81',1,4,NULL,'[]',0,'2018-08-29 19:12:19','2018-08-29 19:12:19','2019-08-29 11:12:19'),
	('5304f01d7ed1ba054989f0739876160d170f024a40a32125d552ead2fc51f59b1b7f254b5af1b514',2,2,NULL,'[]',0,'2019-01-10 00:21:23','2019-01-10 00:21:23','2020-01-10 00:21:23'),
	('530dccc37b1653e4041dec916100a407765ad99804199d35b979740739e62e1e639fcc4a91db0828',2,2,NULL,'[]',0,'2019-01-08 15:55:53','2019-01-08 15:55:53','2020-01-08 15:55:53'),
	('5960b6d53b9c08c3e9e3a71b2b12ec7bc6862c87210b62dc2e5553a698ca7010c9a3f86647de59c1',2,2,NULL,'[]',0,'2018-12-11 13:05:05','2018-12-11 13:05:05','2019-12-11 13:05:05'),
	('5a0bf2730c0c18aa186ffc1de6c6f42e1b6e18c5026e2e81a83a30345454db8e56419d24f8e6f85f',3,2,NULL,'[]',0,'2018-12-12 16:07:05','2018-12-12 16:07:05','2019-12-12 16:07:05'),
	('5cb90d054dd3022b8d6f0360362d7ccdcd28814d55b01ed4fb424158e96df132d7589efb84b382a6',2,2,NULL,'[]',0,'2019-01-08 16:01:28','2019-01-08 16:01:28','2020-01-08 16:01:28'),
	('5db183332f36b89e9776f214ae61aa935ecc49ffd44dbb41ae995851ea080386a480a3da01826be2',2,2,NULL,'[]',0,'2018-12-02 14:37:39','2018-12-02 14:37:39','2019-12-02 06:37:39'),
	('60d85c675bc92f88a3b6085eb56a07e5fb0ebbf28f9fae1ac11e6b98f3a37d091d5f46f78930642e',1,2,NULL,'[]',0,'2019-01-08 15:33:15','2019-01-08 15:33:15','2020-01-08 15:33:15'),
	('678f83a5ef47dca35835fdfa6d7ed8254d6ff47a33dd0f5eb54f68bf9ec4fd7936a443a07a00e71c',2,2,NULL,'[]',0,'2018-12-12 15:29:40','2018-12-12 15:29:40','2019-12-12 15:29:40'),
	('682b1740c7d246c224467edf0b82c383e592ba87f843ce73e729668572e75d04d81897dec4decf50',1,2,NULL,'[]',0,'2018-11-27 21:45:10','2018-11-27 21:45:10','2019-11-27 13:45:10'),
	('69f1b4b1f47e12cd78673339905e38c0e4175aee8231f9b8f483742d3018824e00308642e948631c',15,2,NULL,'[]',0,'2018-11-18 18:37:02','2018-11-18 18:37:02','2019-11-18 10:37:02'),
	('6e793f421f79963fb6bf642b917b7ee22f5be84c50f402a1ec113fb89129692f0878aceb6b7b64b1',1,2,NULL,'[]',0,'2019-01-08 15:24:57','2019-01-08 15:24:57','2020-01-08 15:24:57'),
	('718eca15ef5202beacf6f29831797a66f257ba85c05f960641b187490c3a64737a327c8b4336020d',2,2,NULL,'[]',0,'2019-01-09 21:39:03','2019-01-09 21:39:03','2020-01-09 21:39:03'),
	('72cf76aa3bb5cbadaaf818232f52d63d881b2ddee22f657d2adfdaa5e00560cbe6e5faa9d0ec767a',1,2,NULL,'[]',0,'2019-01-08 21:01:33','2019-01-08 21:01:33','2020-01-08 21:01:33'),
	('740946da3538e92b481e80643dd6a1503cf191d595a50c53a7e5aa729fc270316354813acc5efd98',1,2,NULL,'[]',0,'2018-11-27 21:40:12','2018-11-27 21:40:12','2019-11-27 13:40:12'),
	('744d38eb2c34b31233455a69a3f4b2642021f9168c7be2c2a6bc44c17ad61153c1651c3389debb81',1,4,NULL,'[]',0,'2018-08-28 19:17:54','2018-08-28 19:17:54','2019-08-28 11:17:54'),
	('761ccda5968ce0b302b7ee4f25c2d5eac5718fa753273f6d6684b47787c64add9903f7d18a8c7477',3,2,NULL,'[]',0,'2018-11-28 22:30:18','2018-11-28 22:30:18','2019-11-28 14:30:18'),
	('79850557fcec7e11b039701ca5adf67b26d21ac092b87f110ed2ac942902ae219bf4d223ce03ad0b',1,2,NULL,'[]',0,'2018-12-15 10:26:59','2018-12-15 10:26:59','2019-12-15 10:26:59'),
	('79e32438829a885efd70fb5955e384a64370444af9f8babca8bdeabd58592311e03f5bc482963c1f',1,2,NULL,'[]',0,'2018-09-04 23:42:30','2018-09-04 23:42:30','2019-09-04 15:42:30'),
	('7b2494b7e698368d21a0e84e5b4fe36d7cf6e2f9257d5b8e3289a6cd88dfe03cefe972300b5512c4',12,2,NULL,'[]',0,'2018-11-18 18:22:47','2018-11-18 18:22:47','2019-11-18 10:22:47'),
	('7da655132b26e84af2eed8d3b24f8201b6e37306af56964e91619f7bba5e3cfb5a42d137efa3fb61',1,2,NULL,'[]',0,'2018-11-27 21:30:34','2018-11-27 21:30:34','2019-11-27 13:30:34'),
	('7eaad727666e830be4e4eb87ce9c7820d55efdb984e952b6b16ec42ceb03329ce7f5feda6a3bcbea',2,2,NULL,'[]',0,'2018-11-27 22:02:30','2018-11-27 22:02:30','2019-11-27 14:02:30'),
	('8200b6d1e5778ccbb83cae726ef640ba7df81f603f359b5ed218e80409d19bed79d26c339ac0b18d',1,2,NULL,'[]',0,'2018-11-27 21:49:27','2018-11-27 21:49:27','2019-11-27 13:49:27'),
	('82b8976c734eac3457ede00fa3a94ab8f9b9c29a071125a4fed6334db5b710899e901f2d29bca46c',2,2,NULL,'[]',0,'2018-12-11 12:47:54','2018-12-11 12:47:54','2019-12-11 12:47:54'),
	('867c570c4e45fb4ef9cd97ad4f96a5f845b658155352685f8284b30a7f97bc948362ff6667896fa1',1,2,NULL,'[]',0,'2018-11-28 22:20:31','2018-11-28 22:20:31','2019-11-28 14:20:31'),
	('892626a2f31ee4b2e4f1237b7f5a4c7dfcccc8181e4209fe263e09aac67af8e4f40625475301df99',1,2,NULL,'[]',0,'2018-12-02 14:38:44','2018-12-02 14:38:44','2019-12-02 06:38:44'),
	('8c352ba6c8e9c142b559238ae36c19bdce40d08931d3029392a2df5eae025cc477a8986d0df822e2',3,4,NULL,'[]',0,'2018-08-29 22:05:40','2018-08-29 22:05:40','2019-08-29 14:05:40'),
	('8de0c807a29abadd32c2b84170d906fe7f0373025670735833ba50cea8f851ded1339424b59d6817',1,2,NULL,'[]',0,'2018-12-11 13:03:26','2018-12-11 13:03:26','2019-12-11 13:03:26'),
	('93366bfa742bff8f27f66a4658ec5a4848b693eab1ce49baa62054fab95898604481affe105946fc',2,2,NULL,'[]',0,'2019-01-08 21:49:25','2019-01-08 21:49:25','2020-01-08 21:49:25'),
	('9601fd73835da9e1c8d2a3a2b5197e2669c440953532962da33a868e0e873f4a74843dadf2a3405b',3,2,NULL,'[]',0,'2018-12-11 13:10:39','2018-12-11 13:10:39','2019-12-11 13:10:39'),
	('9607059a6aa0e040696f4c8584222135fb1407a9ae56396a215107e826e1038b9734ca62400602b4',1,2,NULL,'[]',0,'2018-12-27 05:37:14','2018-12-27 05:37:14','2019-12-27 05:37:14'),
	('96e94b3fd8b520d611e2ca5764529a1bd50d837c0663776f7eac58d892719ff2927881a5414120f4',1,2,NULL,'[]',0,'2018-10-06 03:18:33','2018-10-06 03:18:33','2019-10-05 19:18:33'),
	('97d7ecbc6fd16b034b90ac2ab7f4814631738dec1b4a8048d6b2c4dc76c31567f857bd80b8acd4e1',2,2,NULL,'[]',0,'2018-12-02 15:16:02','2018-12-02 15:16:02','2019-12-02 07:16:02'),
	('98531286fb2855c2fd6213ded46436dad7340ddc65a2a400d14fb808625fbbf7e52a73a6d4f81ee0',2,2,NULL,'[]',0,'2018-11-27 22:54:40','2018-11-27 22:54:40','2019-11-27 14:54:40'),
	('9abf2a6bc71cbf4e40a74235461d6254f147ba532f58fca2f24cd86af32fcc58f9c271d660c33957',1,2,NULL,'[]',0,'2019-01-09 22:43:06','2019-01-09 22:43:06','2020-01-09 22:43:06'),
	('a05017c00f2382b9c7c2bfe8e5ff2904c0962b8db2dcba0336008fba98b2fe5268f2a3945d9ff4be',2,2,NULL,'[]',0,'2018-12-27 05:35:56','2018-12-27 05:35:56','2019-12-27 05:35:56'),
	('a59bfc85bc7879b375bd2c268e5306f0e1a5b1e5e1e364af6f09f0e12eb0bfe6b3385df558ea63a9',2,4,NULL,'[]',0,'2018-08-29 19:53:59','2018-08-29 19:53:59','2019-08-29 11:53:59'),
	('a661623d37c4e657a250403acd37edd7bbc4d845c99bb6faea9097a27822bee466a51964765fc870',19,2,NULL,'[]',0,'2018-11-18 23:48:27','2018-11-18 23:48:27','2019-11-18 15:48:27'),
	('a81620862108faec3cf6e85b3142c16fa0d27b72823b289fdb34ab534df5eacd6b1c3b86fb0489af',20,2,NULL,'[]',0,'2018-12-02 16:26:01','2018-12-02 16:26:01','2019-12-02 08:26:01'),
	('a9549ec7e897ffea3f8c6d790be2e7721393e950fbacf1ccef738e8f0a5c8b094995495ba4f3f8d0',2,2,NULL,'[]',0,'2018-11-28 22:19:00','2018-11-28 22:19:00','2019-11-28 14:19:00'),
	('aaadb9c68e4670365ba900c925a863867ec4adc9d25aa550fbe4f4220dd3291801f3091f67830119',1,2,NULL,'[]',0,'2018-11-27 21:52:00','2018-11-27 21:52:00','2019-11-27 13:52:00'),
	('aad687098b86eef4b89f8e94648fbd6120bc3c9635dfd0ef7ecfb107c4821b345d196d207d6881ce',2,2,NULL,'[]',0,'2018-12-11 12:35:27','2018-12-11 12:35:27','2019-12-11 12:35:27'),
	('abb5fb1d0eed889b74b5bfa701c18aa3b613a56a3aed60f7449a50c394c43b571011dbbca77dfe5c',1,2,NULL,'[]',0,'2018-12-12 15:24:15','2018-12-12 15:24:15','2019-12-12 15:24:15'),
	('ac0c869ed498d15ebdeb06c0a28e962a9028bd96c493971cf77847d97c691457580c7d2504490eee',1,2,NULL,'[]',0,'2019-01-08 15:33:03','2019-01-08 15:33:03','2020-01-08 15:33:03'),
	('ac1848b039ae26f4b9ee9bbfc229296439358bc6d468ce0a90004af301899ec0c554716aedf596fe',2,2,NULL,'[]',0,'2019-01-07 23:36:22','2019-01-07 23:36:22','2020-01-07 23:36:22'),
	('af53841930f3425638b51cda51a4f58ba561d093f6eaead4e3272c1196f0a0938f435e31a2f154d5',21,2,NULL,'[]',0,'2018-12-02 16:27:35','2018-12-02 16:27:35','2019-12-02 08:27:35'),
	('b244f186fba6934e0a4854dc73e2740bcb5b79ed0ae44d06bc6d377497aba6d8bf7acc71bdc8f664',1,2,NULL,'[]',0,'2019-01-06 01:58:18','2019-01-06 01:58:18','2020-01-06 01:58:18'),
	('b30144b7dcdd4b963f0ecf8f9b693066d64658b12e3a1faa69e8283e7c901485af8907afc0a55fc9',1,2,NULL,'[]',0,'2018-11-27 21:27:44','2018-11-27 21:27:44','2019-11-27 13:27:44'),
	('b4b5670670692785beaaa78bc4b8ee7a6164d3c70ed3b88e9c2ea9a18a291d2b765ba05dc5d00cae',16,2,NULL,'[]',0,'2018-11-18 18:43:15','2018-11-18 18:43:15','2019-11-18 10:43:15'),
	('b4f11a28a1352abab96d9f3466ef7e3f731df52092a182b922ebfadfc46f19ba87ee0d4baac3398f',1,2,NULL,'[]',0,'2019-01-07 23:37:06','2019-01-07 23:37:06','2020-01-07 23:37:06'),
	('b5bed012d1f492320c16748e911d4479676624149744cfb92ca0cafeb66866948e70b258e8b400a4',2,2,NULL,'[]',0,'2019-01-06 10:05:21','2019-01-06 10:05:21','2020-01-06 10:05:21'),
	('bf1e7f769003e2aa6f16cba0e60bfc837e1bddddd641bb391bf95fbcf447ed21c6871e36dc1eefc3',2,2,NULL,'[]',0,'2018-12-11 13:07:13','2018-12-11 13:07:13','2019-12-11 13:07:13'),
	('c503579c1317aabf29982fbdc8747d6750bd0abcc92185475c299653fd69ad977c89e3fde505540b',1,2,NULL,'[]',0,'2019-01-08 15:24:42','2019-01-08 15:24:42','2020-01-08 15:24:42'),
	('c52e8f2fb08490e03c86364404eee762b78bd5ba90de7b7db9d58c4573c86ddb1b54d6d13611864b',1,2,NULL,'[]',0,'2019-01-08 22:46:40','2019-01-08 22:46:40','2020-01-08 22:46:40'),
	('c67b9673afe416dd56698897b9c4cf715bd7c513759b19a84665ac5d3345b2a1188b1544a720cf26',3,2,NULL,'[]',0,'2018-11-28 22:37:29','2018-11-28 22:37:29','2019-11-28 14:37:29'),
	('c7935813e807dbac467f3b8046c5ef87d567309c703cd98b20c38b0f729b9df54be3a65440c534f1',1,2,NULL,'[]',0,'2019-01-06 10:05:46','2019-01-06 10:05:46','2020-01-06 10:05:46'),
	('c82e27873f2822c1a3d7055773914e15449dabd644759ceffcd81621433529bf09c4f5be5f9db251',1,2,NULL,'[]',0,'2019-01-08 16:06:18','2019-01-08 16:06:18','2020-01-08 16:06:18'),
	('c9057c7b9c3dd3ba0ef501ce41658dc25bf73a05d234b442c0059b0a6013d499aea20fc7b2f646a4',1,2,NULL,'[]',0,'2019-01-08 15:36:04','2019-01-08 15:36:04','2020-01-08 15:36:04'),
	('ca022e4b4521b7a0729db6256cc81f17ff98051586bf15a5554dbd394884315c8c785c1e7168b2b6',3,2,NULL,'[]',0,'2018-11-28 22:31:32','2018-11-28 22:31:32','2019-11-28 14:31:32'),
	('ca9cc6e290f0fb4f33159a2a8b665fd595b68291688978f895e61fc0b4c44da6fd6ec149c71ffa25',2,2,NULL,'[]',0,'2019-01-08 21:12:32','2019-01-08 21:12:32','2020-01-08 21:12:32'),
	('cb280d91eb45d044962f6aa18092742c09dedf21c8dbad8c2aa57748c4e3993be77e9b9ea77191f1',1,2,NULL,'[]',0,'2018-12-10 14:08:16','2018-12-10 14:08:16','2019-12-10 14:08:16'),
	('ce9338a41f4da4455758daf4d9657c123c54e04ec3948f563c4f08f33a68c4e3438ef4932334450c',2,2,NULL,'[]',0,'2018-11-27 22:52:04','2018-11-27 22:52:04','2019-11-27 14:52:04'),
	('d2021ffc821d55aaf641ad554602e6c629df4f94f4cf6a997e0b46f02084b8e3a8cf7874c31ba9e5',2,2,NULL,'[]',0,'2018-11-28 21:21:49','2018-11-28 21:21:49','2019-11-28 13:21:49'),
	('d51f0b2eff60f507e655a77f65c8b875e51abaff7738cf359e2ac80b0f429fed2aab533f7ee6d918',1,4,NULL,'[]',0,'2018-08-28 19:17:21','2018-08-28 19:17:21','2019-08-28 11:17:21'),
	('d522034eb3567f091d3434f2590f31f80b9d1cd5ae4d5282068c2a76fb49eacb4f5c5cc973d224e1',1,2,NULL,'[]',0,'2018-09-04 23:40:26','2018-09-04 23:40:26','2019-09-04 15:40:26'),
	('d52a8efece0036c0d096b53254d516ecb8612cf808f25e7e7e3aed9d296fa240afef67413105817f',1,2,NULL,'[]',0,'2018-12-02 15:23:13','2018-12-02 15:23:13','2019-12-02 07:23:13'),
	('db3493ca48ea40dbed07ca530af9ccef3c3fad9065c6449be756cbc44a8a5e0dbb4684fc7d6aa2dd',1,2,NULL,'[]',0,'2018-11-28 22:31:08','2018-11-28 22:31:08','2019-11-28 14:31:08'),
	('e02951274882ab3243153b494fa345ee363edde17f8cd212b1463690cab16a294ba6f42841f7be55',1,2,NULL,'[]',0,'2018-12-11 13:04:35','2018-12-11 13:04:35','2019-12-11 13:04:35'),
	('e09a84800e859ef0eab8f6c74a875ee7add6afbc78489b215dc4f266f342241662feb2780df04fef',1,2,NULL,'[]',0,'2018-11-27 22:52:34','2018-11-27 22:52:34','2019-11-27 14:52:34'),
	('e458bb611732c2f90c94e0f9ff659c8d2ccc058d0144e22636cfe8202c3b81913cd32e92eafc604f',1,2,NULL,'[]',0,'2018-11-27 20:55:58','2018-11-27 20:55:58','2019-11-27 12:55:58'),
	('e7d9df21888e9aa3baf7c480a0f01f679a7308138c3a5ac1feb03d54054deb4543304ae2caea7934',17,2,NULL,'[]',0,'2018-11-18 18:53:41','2018-11-18 18:53:41','2019-11-18 10:53:41'),
	('e8f4d1596f3fc6024b66fec272acbfffcdda2bb1f817917a3b983a1897b8a202d5a9ec1cce5655e3',2,2,NULL,'[]',0,'2019-01-08 22:50:48','2019-01-08 22:50:48','2020-01-08 22:50:48'),
	('edf6478d111024b4b3da8305c28a56cdfbb39dd9ba99a61c1c44f04282aac2b2b2a83511807279fc',1,2,NULL,'[]',0,'2018-10-06 01:08:04','2018-10-06 01:08:04','2019-10-05 17:08:04'),
	('f0ea2eb03d8b7605ff24a7125ad238964e5c680d266fcb86cb05f4b8fcbbe7449ea9803e3511927e',1,2,NULL,'[]',0,'2018-10-21 10:55:07','2018-10-21 10:55:07','2019-10-21 02:55:07'),
	('f651a35d030fd9026242140b3232854271206290152c0ccff4a62c5e7ee0fdb2ef1dfe26d0880326',26,2,NULL,'[]',0,'2019-01-09 21:24:58','2019-01-09 21:24:58','2020-01-09 21:24:58'),
	('f7bd3605fa96a77697ab11696572406f9fe68bb5a79392f81aace9f9bee4baa9c2bc969eff63aed9',2,2,NULL,'[]',0,'2018-12-02 16:35:19','2018-12-02 16:35:19','2019-12-02 08:35:19'),
	('f9971dd3b3034e00ed774f31a1f3f4f320701196d6ebcd3315776288552381e01c88cc7595247b7b',1,2,NULL,'[]',0,'2018-12-27 05:34:21','2018-12-27 05:34:21','2019-12-27 05:34:21'),
	('fd0e6abdb42dcd6c76920b20e4b3426d3476881c7110482ebebb3cfd11d7e69e3a8433bd5fe1d705',3,2,NULL,'[]',0,'2019-01-07 23:36:41','2019-01-07 23:36:41','2020-01-07 23:36:41'),
	('fd98bfae353ed5295825475105c48c8913a8c4a1065ebfb7e34fb097a6c22e8dddd9a41582c6c8a9',1,4,NULL,'[]',0,'2018-08-28 19:18:02','2018-08-28 19:18:02','2019-08-28 11:18:02'),
	('fef91fa3e41aed5554d880c72a761a126df013b6deae9ce7adc51f26aed1e6d91b1972e1b4a3d5b1',22,2,NULL,'[]',0,'2018-12-02 16:29:42','2018-12-02 16:29:42','2019-12-02 08:29:42');

/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table oauth_auth_codes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_auth_codes`;

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table oauth_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_clients`;

CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`)
VALUES
	(1,NULL,'client_1','Fwvx1kWCvdHKlU57AdgIfpiuoaNJfIoI27Glun5I','http://localhost',0,1,0,'2018-08-28 19:09:00','2018-08-28 19:09:00'),
	(2,NULL,'Laravel Personal Access Client','secret','http://localhost',0,1,0,'2018-08-28 19:10:48','2018-08-28 19:10:48'),
	(3,NULL,'Laravel Password Grant Client','Q3Dmn8x1VMouKNcCtpMJIEwyJBiidvT2f9IIrKv9','http://localhost',0,1,0,'2018-08-28 19:10:48','2018-08-28 19:10:48'),
	(4,NULL,'client_4','Z6vYb7hXZmtRihMyWc3IwLp7wEUzj3y1vPcbGmhQ','http://localhost',0,1,0,'2018-08-28 19:14:23','2018-08-28 19:14:23');

/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table oauth_personal_access_clients
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_personal_access_clients`;

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`)
VALUES
	(1,2,'2018-08-28 19:10:48','2018-08-28 19:10:48');

/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table oauth_refresh_tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `oauth_refresh_tokens`;

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`)
VALUES
	('01fdb63b439ad6d9130957f95d669ed617bce24b9fafba4a0ccddfdf7289bf713dfea8f434c630a1','744d38eb2c34b31233455a69a3f4b2642021f9168c7be2c2a6bc44c17ad61153c1651c3389debb81',0,'2019-08-28 11:17:55'),
	('04c04f26c71392634982b88fc4f9a88293a917c4fadfd7fcfc41477868afa8914d6a87e4b5c11263','05531b0bab6a5627a83ff4a99c1564589ae63174d16f0b36bb884d16952cb247cab43f86820dd140',0,'2020-01-06 01:57:51'),
	('04d0564e4d2c74884f4c7fefad8e07af6f561de4396449f2921de3081821093aaa287f8f5165c5fe','a661623d37c4e657a250403acd37edd7bbc4d845c99bb6faea9097a27822bee466a51964765fc870',0,'2019-11-18 15:48:27'),
	('0893193271b3797273f6757b2e886e36fd768ce191c18648fe42ccd3c81cc4ba15ee259e9dbb03d3','aaadb9c68e4670365ba900c925a863867ec4adc9d25aa550fbe4f4220dd3291801f3091f67830119',0,'2019-11-27 13:52:00'),
	('0a45dfd1d82331b3acb7af406b82bead64a46cd4db4007dabbbdf4a17f59305031b82ff53b358930','ac1848b039ae26f4b9ee9bbfc229296439358bc6d468ce0a90004af301899ec0c554716aedf596fe',0,'2020-01-07 23:36:22'),
	('0b10fee1118ef9d67aeda94828bf048906464a56dac4039359cc78e1de6a904dde54ca899aaa417b','1db4d6b31033572faf18904d69d9d52b0a7f8217a5aac677fec685fe42275a0cb596dfa794bc358e',0,'2020-01-08 15:57:48'),
	('0d7fd6bebdb90aef535366a5091916ccddb7986830a6e22b24a4dec71ce718eb2a140e056b4b6f2a','328ccd54acd0101572fda19e66d5ca2bdb4f86b40f45fa2ee7c021ccb52fdcca44c6989a014dd60b',0,'2019-11-18 10:23:28'),
	('169d8ebe7e18d4a4c78e9c52dab79c23b86733de88b99477c2ffcb3bd1077aa6b9b3c260b6bc6a57','14e0fc21e8f914c5202ac954413a5ed12748e8254a5ee2ff9e5905a87ee52af5e8894fc324fc2002',0,'2019-12-27 05:38:02'),
	('17d412c95deddb68ae85c5796ef32e77cb27336136e2cdd869d82aa5ba20ec6c5e09661e760f3155','0059122c2f1ead28b23ad9c7cc150edd26ac3c38961f54415b541ab2017f4070ba3d078d09196556',0,'2020-01-08 21:49:47'),
	('1c5f61911e7ef07ee122b8f36d7be750217f25a6fec37b3623b7aeac32785c80e6d242d6abede6c1','abb5fb1d0eed889b74b5bfa701c18aa3b613a56a3aed60f7449a50c394c43b571011dbbca77dfe5c',0,'2019-12-12 15:24:15'),
	('1ea7dd0e3a4792dff24cb36f0333f257754c66ac01fb048f519340eedce8c7859b6e708b1a7b3050','740946da3538e92b481e80643dd6a1503cf191d595a50c53a7e5aa729fc270316354813acc5efd98',0,'2019-11-27 13:40:12'),
	('1fc503e2ce5dec71e0f30d2f2cd61b6c05bb78d214435eca899900e25b7e0f037b0892c84afd7bbd','98531286fb2855c2fd6213ded46436dad7340ddc65a2a400d14fb808625fbbf7e52a73a6d4f81ee0',0,'2019-11-27 14:54:40'),
	('2379d64dcbf31ee21d5ea5fe2535039aec37f3f1cb425d5062de2d02bf6a67c599c29eae53cc6f21','2ea4b3d9e24566a40b2279aea121c37d20783cbd0a0c75a06bce116e36e61ef45b883f0d18063058',0,'2019-12-02 08:18:40'),
	('243402833d51aefa1f9226a5c9ba66e01c63bed0dd46077fb9912ffeadff479089068afb81ea5498','69f1b4b1f47e12cd78673339905e38c0e4175aee8231f9b8f483742d3018824e00308642e948631c',0,'2019-11-18 10:37:02'),
	('29e611ef0ad599825584939ea082de80f92e5550b887fdd9ddfe43a71689008266f9e08a9106810c','af53841930f3425638b51cda51a4f58ba561d093f6eaead4e3272c1196f0a0938f435e31a2f154d5',0,'2019-12-02 08:27:35'),
	('29e6d479be6f88752f38a8ebf81a60b8b8c0d265f735225614e08b85f8f505c5ba128370c949cce7','3cb4be1da47fcabfbf6807097a07ca2c09d299a8b326cec664acac2a009c9e93fc8ca702c853e795',0,'2019-11-27 14:40:15'),
	('2ad149ff0bccac22bda8253e50b3392b040e30262a47e34a524649bd8cf605dff245a98fae1cf4f5','a59bfc85bc7879b375bd2c268e5306f0e1a5b1e5e1e364af6f09f0e12eb0bfe6b3385df558ea63a9',0,'2019-08-29 11:53:59'),
	('2b64e3018ce901e3bda87b4a0c7b936d1ac3317656fe7cf1cbea85fdfb5e40159e902f59290357e9','c82e27873f2822c1a3d7055773914e15449dabd644759ceffcd81621433529bf09c4f5be5f9db251',0,'2020-01-08 16:06:18'),
	('3150cc2a348b19eaaef38b51df19a9b61fc1aa4d402912951b547a380412724be6874ad195c9b1a3','bf1e7f769003e2aa6f16cba0e60bfc837e1bddddd641bb391bf95fbcf447ed21c6871e36dc1eefc3',0,'2019-12-11 13:07:13'),
	('346469c8ab332616acc1ea6859009dfea70f7bd8b85efebff8776731b6d69e84ec97a4e81e1afe5c','349f767429274d8be3c9d1f34ebd207fc59ff5a53a36793db5b90ec23477bd456f7459ec5a44ad6e',0,'2019-12-11 13:03:54'),
	('35b00f33343770caf6bc11b046f487e0f6db7b50103db47b57c07c80e24ae29b4a3b9ff3738c83ca','96e94b3fd8b520d611e2ca5764529a1bd50d837c0663776f7eac58d892719ff2927881a5414120f4',0,'2019-10-05 19:18:34'),
	('35e9808745fd86cde1cb556fd88ed637b4b0750b9cb336b409a1a8d77041231fde39e9801265013b','0dfd45786739a427439ff49f8eab0edaca5d59b2d0e5a72ae4412c654c0dcf07e5c2a15c2ea160a2',0,'2019-11-18 14:52:56'),
	('361cdb393a2f65d5822669a875968dc6002c0ccd9dc1eb9c239c07f989aa3d1403724891872ddefe','ca9cc6e290f0fb4f33159a2a8b665fd595b68291688978f895e61fc0b4c44da6fd6ec149c71ffa25',0,'2020-01-08 21:12:32'),
	('3762aceb922420c527a0a9a11685c1e6d11f38f108ff1798aabf509a29bcb759494fc017fdfa8ba4','678f83a5ef47dca35835fdfa6d7ed8254d6ff47a33dd0f5eb54f68bf9ec4fd7936a443a07a00e71c',0,'2019-12-12 15:29:40'),
	('394f406d44d6ae015c1aff446aa39d27f6503922f419c107ddf14ef2562027e4fd87ca3e894ea132','761ccda5968ce0b302b7ee4f25c2d5eac5718fa753273f6d6684b47787c64add9903f7d18a8c7477',0,'2019-11-28 14:30:18'),
	('3b9a57810f31c1150cd36db58ce8e81a9934543da448f76961ab6f94c3565164cb1d0dcbd693e484','fef91fa3e41aed5554d880c72a761a126df013b6deae9ce7adc51f26aed1e6d91b1972e1b4a3d5b1',0,'2019-12-02 08:29:43'),
	('414e90460ab77b484df6892c6abaf3671a9d85880bc996869022cbc2cc37dcfde3958788e0d0794b','f9971dd3b3034e00ed774f31a1f3f4f320701196d6ebcd3315776288552381e01c88cc7595247b7b',0,'2019-12-27 05:34:21'),
	('420f8671a220c209d95c877f883da9b7e6b16affb1004820d08206d655fe2f032e969cdd9a84f64b','a05017c00f2382b9c7c2bfe8e5ff2904c0962b8db2dcba0336008fba98b2fe5268f2a3945d9ff4be',0,'2019-12-27 05:35:56'),
	('42e909306175f80230b1f674771422e9bb588eb79d7c728383c6e9dbfc41e98697ccce3439c096bb','b30144b7dcdd4b963f0ecf8f9b693066d64658b12e3a1faa69e8283e7c901485af8907afc0a55fc9',0,'2019-11-27 13:27:44'),
	('434b383dd8ed43704fb88e5f986a8b003470c235876a1b0acb6ea444c102ae1a2f14b1b24787801c','ac0c869ed498d15ebdeb06c0a28e962a9028bd96c493971cf77847d97c691457580c7d2504490eee',0,'2020-01-08 15:33:03'),
	('437010716e6b1f7d980335627c2abd0b53c95ef7deb530d59c6e4129c720ff066d47156749cb9c54','e02951274882ab3243153b494fa345ee363edde17f8cd212b1463690cab16a294ba6f42841f7be55',0,'2019-12-11 13:04:35'),
	('448612eb12f9eefafb5720c7e4e5ebf48171a0a2710ad39b6f72c1a412052e3ae602a933a7950925','040524ddd4409cfd752730205c137b2cd4a4a4efb58b81e3e189ccd392565a697131a46ce98cd2ab',0,'2019-12-02 08:26:25'),
	('44acedf121b316e1df0258dceb52b788793a6b6eb47841fb514c65088ab7a5b03f0faf53da3b73a1','b5bed012d1f492320c16748e911d4479676624149744cfb92ca0cafeb66866948e70b258e8b400a4',0,'2020-01-06 10:05:21'),
	('4650a5c2ebada267d6f06335fab5703b8cd438995bd28c70850ddc21846fce0668c76357a7c9b784','97d7ecbc6fd16b034b90ac2ab7f4814631738dec1b4a8048d6b2c4dc76c31567f857bd80b8acd4e1',0,'2019-12-02 07:16:02'),
	('480c83abde720b1695e8f1630e954be5c8b70c1b6074fd1d70ffb1e8d10be90b82caa7ea058be843','f7bd3605fa96a77697ab11696572406f9fe68bb5a79392f81aace9f9bee4baa9c2bc969eff63aed9',0,'2019-12-02 08:35:19'),
	('482e7a240ec3108f94b7bd339494b8a91af656880041eb4668b36b9227cf7eb3f04d873afa3538f8','b244f186fba6934e0a4854dc73e2740bcb5b79ed0ae44d06bc6d377497aba6d8bf7acc71bdc8f664',0,'2020-01-06 01:58:18'),
	('4ca39d6e7c78c6c2f8f9d79dcf7a4273bfb68d411c5e62d9945cf7f398f2c6434f94419d7d6f2796','ca022e4b4521b7a0729db6256cc81f17ff98051586bf15a5554dbd394884315c8c785c1e7168b2b6',0,'2019-11-28 14:31:33'),
	('4de2e38205bca698df05b0f1b56602778ec787260b429a9e8ebd7d79ac6e7e081ef6f2e65ccbc242','021e408daa908c49af8a2bd20b7ecff14d6301c13f1b2c0aec6ae09fcdb0e6ada2ed2bd423e6ab0d',0,'2019-11-28 14:20:57'),
	('50e49595362e8d86d23f4fc52ee85caa9e641da392fa4a8a31192da14f8b42c35c98f00535bb10e8','a81620862108faec3cf6e85b3142c16fa0d27b72823b289fdb34ab534df5eacd6b1c3b86fb0489af',0,'2019-12-02 08:26:02'),
	('56af15400464390d19cf106d94c946e02412223d374509bd0598dfca7412b43bec2f87d5ed880262','db3493ca48ea40dbed07ca530af9ccef3c3fad9065c6449be756cbc44a8a5e0dbb4684fc7d6aa2dd',0,'2019-11-28 14:31:08'),
	('56ba54b4c636477a951f6f22c2b9e2b4b55c33b4d655291ed2d7de66526715c6021b58ed1ff5ed0c','aad687098b86eef4b89f8e94648fbd6120bc3c9635dfd0ef7ecfb107c4821b345d196d207d6881ce',0,'2019-12-11 12:35:27'),
	('5ce01988148d7fe7795fdbcee117ad02347dc5d7b2aec27b19a72f233fbae6f191ff728fb42dabec','c52e8f2fb08490e03c86364404eee762b78bd5ba90de7b7db9d58c4573c86ddb1b54d6d13611864b',0,'2020-01-08 22:46:40'),
	('5d5efbdd8ff76512b7dd8047bd5af815582bc4a1aa638b8a87a3239eb5d3cde27c3e1e561bdc6b29','c9057c7b9c3dd3ba0ef501ce41658dc25bf73a05d234b442c0059b0a6013d499aea20fc7b2f646a4',0,'2020-01-08 15:36:04'),
	('5f9265b06919d0dd3665959e5972c308b75ff094c27fcfa1a570f77b830929096804b8cac429d4c3','cb280d91eb45d044962f6aa18092742c09dedf21c8dbad8c2aa57748c4e3993be77e9b9ea77191f1',0,'2019-12-10 14:08:16'),
	('6469c5d78a0bd2066156f81940bc0d2f5564336a0c0949766413a36abf97b400d6ebd715c20552de','c503579c1317aabf29982fbdc8747d6750bd0abcc92185475c299653fd69ad977c89e3fde505540b',0,'2020-01-08 15:24:42'),
	('67d2ba0a4717a8a251c9e9f65778a6182674f7e2bcb83584a1fae926f9b0664af4a2fae324bbc96f','e09a84800e859ef0eab8f6c74a875ee7add6afbc78489b215dc4f266f342241662feb2780df04fef',0,'2019-11-27 14:52:34'),
	('68a1d3b947ce7e7a2ae341289de452f0891d90f88779f9431cc223ae93cbdafa73915173fd035ed7','9abf2a6bc71cbf4e40a74235461d6254f147ba532f58fca2f24cd86af32fcc58f9c271d660c33957',0,'2020-01-09 22:43:06'),
	('69ae5f1690a9a5c1775b2668efb63c1f75c2c2b3281e816faa67e57eede7e807495474f5ca89b695','1f56bc943a7985e97a1e10fc3bd6d399bed1f270578f88bf60681dcf7b2b5715ba2948e34c66aab2',0,'2019-11-28 14:24:19'),
	('6c78c64478c4a051eb4432f35aaf816a10f9069690bd2de860a153e6f786138d9ed3f9e0b2f9847c','39e5dcd8314a2d30f7e26c3e5fecab188dd94fe314b8b9de5d1c34025da11ea521fc08452f2ef1d2',0,'2019-12-11 13:06:38'),
	('6c8a85054c69686bb2c211dcbf75f738104be0eed822a17670f56340511203d292891987e78ce61b','8de0c807a29abadd32c2b84170d906fe7f0373025670735833ba50cea8f851ded1339424b59d6817',0,'2019-12-11 13:03:26'),
	('6fad0a90712efe3dc9ff124902f231dc303ad75ecbcd3cbb8a0659e007f7130a14d2899d01edb45d','b4b5670670692785beaaa78bc4b8ee7a6164d3c70ed3b88e9c2ea9a18a291d2b765ba05dc5d00cae',0,'2019-11-18 10:43:15'),
	('754681b7d217efecef8cf593529347f460880a9029d493b4b95406f50c191a363cc2ed58cf11fa65','530dccc37b1653e4041dec916100a407765ad99804199d35b979740739e62e1e639fcc4a91db0828',0,'2020-01-08 15:55:53'),
	('75d1a54810435cccc28a998dc22ebdae76cddfd73fd206db7cb86edbfbd324c040bba70317202008','93366bfa742bff8f27f66a4658ec5a4848b693eab1ce49baa62054fab95898604481affe105946fc',0,'2020-01-08 21:49:25'),
	('78f4d82ad4e57d492267f2a8ddd91895a1877c10c430999c643e58603c3a9a05b78c2ad0c724c377','0e35b43efb7e0d0a2f71f30e00990cc3d7cd3474abab65cb224c71ecf4b85cef8d2e52b15c39ba0d',0,'2020-01-11 10:24:44'),
	('7a2ccbe19476bcf49b99ce1a174e2fdf86b88d574c2e6776facd6a27c908fad82dbf9e53e9cd60a0','19a1bca9a0012b5a2ad079585e1bf626f9e66d0ab2976c89fe16ed8e0582aa3fe3c1ad7d2a5457ee',0,'2019-12-27 05:32:08'),
	('7b126672cefd48eccf0e8d2cfea02b77f9180c7b30c9de16102fcfcb2a2969b5e154c735e95066ca','867c570c4e45fb4ef9cd97ad4f96a5f845b658155352685f8284b30a7f97bc948362ff6667896fa1',0,'2019-11-28 14:20:31'),
	('7c50745c3a742087a6d1be413a80beddca5a685a9a6a19f6277ab99ffe8c608f0c98a86ce9bbc062','fd0e6abdb42dcd6c76920b20e4b3426d3476881c7110482ebebb3cfd11d7e69e3a8433bd5fe1d705',0,'2020-01-07 23:36:41'),
	('7c717a32df2bdba5596afc8d1b6ad9b862eccf7825bacbea88a694cd27663d4b6bd66ce2c4e1a0b9','3e8895402d602bc037326ac12f37eaf71809fe492c35708fa218202937ed8f92ed62a7d87702d637',0,'2019-11-27 13:59:18'),
	('804c379f5e5f5e333a446ea1d67091126f21bd15518bed0b49211c906b1ea9835a55d7b9a8aa3eec','347a79ed57aa8fda0674dfad466f9427419bc4840a0aa09c0007493cf072dcbdb7646c6ae932b9b1',0,'2019-09-04 15:42:21'),
	('808dff6046939c5f1db7ad96f3b23ef9d67687c6bd38af932709138d95e9ac4d860e8537c2d22dfd','0c0b7380162f42c83f9644c18f53c441e09b61f9680d53427c8e26a635238acc57f80d7180de4b41',0,'2019-11-18 15:07:17'),
	('8145cdc0b8a5d6c1fde865c0cfe1c61bc4be25a8c28de72b6fe4d21440077c1908e3f8a9eaa255f0','d51f0b2eff60f507e655a77f65c8b875e51abaff7738cf359e2ac80b0f429fed2aab533f7ee6d918',0,'2019-08-28 11:17:21'),
	('82285a4062868349f2039d4668edf1ddd6a64d5c4ee4452fd7b76db3110682a0212895917bd4fded','c7935813e807dbac467f3b8046c5ef87d567309c703cd98b20c38b0f729b9df54be3a65440c534f1',0,'2020-01-06 10:05:46'),
	('84b42455a4b2912af4479a081af591778bc313fe5c201511e985c18b4300aa9a39325ce50cba4ec5','a9549ec7e897ffea3f8c6d790be2e7721393e950fbacf1ccef738e8f0a5c8b094995495ba4f3f8d0',0,'2019-11-28 14:19:01'),
	('84b5ab2e72ec4e403f34f49a2673d7cd56d8d4bd344e212f09623a4ec820b140b97bc0cbff905cbe','9601fd73835da9e1c8d2a3a2b5197e2669c440953532962da33a868e0e873f4a74843dadf2a3405b',0,'2019-12-11 13:10:39'),
	('87163eed306e5e2d304e404153347b74ecf2c0d57bbe3409605a2cd01c221e1a249b3efe7e5a60a1','5a0bf2730c0c18aa186ffc1de6c6f42e1b6e18c5026e2e81a83a30345454db8e56419d24f8e6f85f',0,'2019-12-12 16:07:05'),
	('8a39fed82e654d05cc87360c8f398939dd183036beb0075ff563e60652fbb335fb96859845de458a','52d9a9f09462e911c3d9b1a4f8260685644d6d7c16d6655fa802bced55034cf82d72744566696d81',0,'2019-08-29 11:12:19'),
	('8b44f7a8701f08a625dce9e7e61125471ed46109b42aa7968df88a08ac08cb76c7a004dd8c0b81d0','198ca028df7b9a6d8d2999edf9e609fe1a86854da027364f642eba6c4472ccb020bcaeb72121cf66',0,'2020-01-08 16:02:09'),
	('8b7583f2dd78cfd7f489c74efd9e8aa8d8ad3939ccdfe06a6aed4ea4bcbf963b72b9eee1109402b8','7b2494b7e698368d21a0e84e5b4fe36d7cf6e2f9257d5b8e3289a6cd88dfe03cefe972300b5512c4',0,'2019-11-18 10:22:47'),
	('92d28d74a1efe80a9b18a5d2da60421c9d23fe40cdfc4b77678638732233412d1d546ad30e8a4d47','5304f01d7ed1ba054989f0739876160d170f024a40a32125d552ead2fc51f59b1b7f254b5af1b514',0,'2020-01-10 00:21:23'),
	('92d62602ee1220fa8f833b969542635c875296448c34910144be933edd38c7f5bd98748eede96248','5cb90d054dd3022b8d6f0360362d7ccdcd28814d55b01ed4fb424158e96df132d7589efb84b382a6',0,'2020-01-08 16:01:28'),
	('98e27c857aa1822cf99b2eb79bd1ddc66f359034eb472f38da4a2f244bb568b912807666e21fa752','f0ea2eb03d8b7605ff24a7125ad238964e5c680d266fcb86cb05f4b8fcbbe7449ea9803e3511927e',0,'2019-10-21 02:55:07'),
	('9a9801cb232254463f1250d9be7446e3f12472d5392eca379f287e54b2fe0d6a8ab5a3133c714cb3','5960b6d53b9c08c3e9e3a71b2b12ec7bc6862c87210b62dc2e5553a698ca7010c9a3f86647de59c1',0,'2019-12-11 13:05:05'),
	('9c968f0d4a6ba9983341f6616f6f56813cde19215202867f89cc51903d840c12b9da119344545f3b','053175bd6a0868481f8207ca5799ad9cbfcf24e86083a5a3156f151a353d61cfbfda03b4a956b453',0,'2019-11-18 15:08:00'),
	('9d4bab681c72f76be8d48010c36879eb6d87abb4ba88206f508771355a02e5f47e559ef0aaea2007','72cf76aa3bb5cbadaaf818232f52d63d881b2ddee22f657d2adfdaa5e00560cbe6e5faa9d0ec767a',0,'2020-01-08 21:01:33'),
	('9d9a7322ad8cf526745e996b66db895248589a46471a5a3f55d76ff2bdf59917d72bfc170bcfb709','0715b2ddb482aba2c94a34ae05a9376edcfad9e4ce19a3e7debdfd0f9f774ef0297bfdf65eaa56bb',0,'2020-01-06 01:39:12'),
	('a2b77f15c777eff6135262779bd884bbb7e0c0ad1619d1e83b519d10f9b4950fdf1bd75fae423915','f651a35d030fd9026242140b3232854271206290152c0ccff4a62c5e7ee0fdb2ef1dfe26d0880326',0,'2020-01-09 21:24:58'),
	('a36f74def592e15830450f486b778dbffb40452aac9b331f26a0a035ae420446266bdf3d291cefe3','0942e0c7d68d4e7f6ab4567472681c63f8cc8e3f5b289376766097ec0977391c7210cbecfba93007',0,'2019-11-27 13:46:17'),
	('a5875c2cb9543593033172e998226150165d2b0db038d1b55b12e4710139cd4b5c8d910bd50cb3c4','d522034eb3567f091d3434f2590f31f80b9d1cd5ae4d5282068c2a76fb49eacb4f5c5cc973d224e1',0,'2019-09-04 15:40:26'),
	('aa096e25a84dbcb81420cfec825a39a82368caa81a4353a65c4b039ab2b0a39050e1a04b07f6b6d7','4acfa67e61d2a1e6bffd4442f39211dbce0a328cd5e0c5fe42d6f5859b3b96d50097f208718dec7e',0,'2020-01-09 21:26:49'),
	('aa96549c975296cf4b66e6ace544b5ec36fb71364e55a49b236f041b7a3de960206f1b8cb9cd77fe','5db183332f36b89e9776f214ae61aa935ecc49ffd44dbb41ae995851ea080386a480a3da01826be2',0,'2019-12-02 06:37:39'),
	('abf214c63c59099839121cb732a60ca98dc46488740a9641869be44f124b39ec965ac86f7fc16183','e458bb611732c2f90c94e0f9ff659c8d2ccc058d0144e22636cfe8202c3b81913cd32e92eafc604f',0,'2019-11-27 12:55:59'),
	('b0a1a2dff40d7a751dbd8facfb176010e41f719841b8f725903bf251e6a0399b3366580e6d6480b2','682b1740c7d246c224467edf0b82c383e592ba87f843ce73e729668572e75d04d81897dec4decf50',0,'2019-11-27 13:45:10'),
	('b1c226a6847da833ddbabfd80b1f8b2db77c7a4d6ccaad473ae11a0de9f9f8d33f551cc4da9f5ffe','79e32438829a885efd70fb5955e384a64370444af9f8babca8bdeabd58592311e03f5bc482963c1f',0,'2019-09-04 15:42:30'),
	('b8961f2920bdcb210b9d8969e34f05a6b7f4f21b01b72e56f833c05425f4a17c322b6c65fbe4e650','38327a67d1db463e55d95ce6b21dc8132339c01b902ea9f728834277aa861bc8c12437d870d0c1af',0,'2019-10-21 03:13:32'),
	('b8e48d0cf53e2522df56ac5fbe60df45ab069ae608a959ffb2921e8403b4eb70d9a88773c5641cd2','9607059a6aa0e040696f4c8584222135fb1407a9ae56396a215107e826e1038b9734ca62400602b4',0,'2019-12-27 05:37:14'),
	('ba509964cb57eb3fef0eabda408c5fd92c09eae99e763f1d274fe8d70296dc43078fc74a26a93c33','718eca15ef5202beacf6f29831797a66f257ba85c05f960641b187490c3a64737a327c8b4336020d',0,'2020-01-09 21:39:03'),
	('ba7ba70d5480c894eb9a968093f2618afe68589332db62e7cbbd5eb7295695338f4c8d9e501fb472','892626a2f31ee4b2e4f1237b7f5a4c7dfcccc8181e4209fe263e09aac67af8e4f40625475301df99',0,'2019-12-02 06:38:44'),
	('be255520583fef561d64791e9fb9b97e0ecf73c50793b05cc0fdf2b19f73cf952946e4c7450324cf','226a89b44d5dcaa42fa49c53ff1b6305097d7afbfd11e9367f4e1d01eeb2502ebf3faa4811a012de',0,'2019-11-18 14:57:25'),
	('c0963d0a34fdcc9d507587bf184593e2fe05df4434976da941a8f570d10f06031230b57dd65a9feb','2686917af67f0c6dceae7a3d0858a61d16d0bc4263b7abd7fe7b99cd0a31437d7039d3a76481bd86',0,'2020-01-07 22:22:19'),
	('c09ed8f896c728f261715f0018119925b3f460efc5f595486656b8f3b983eee703b198410668cd47','ce9338a41f4da4455758daf4d9657c123c54e04ec3948f563c4f08f33a68c4e3438ef4932334450c',0,'2019-11-27 14:52:08'),
	('c3edb117c3171c1a1631a1feb8548977d645b71f41279ce1ff320d32a812ef742512a0357f04a6d4','7da655132b26e84af2eed8d3b24f8201b6e37306af56964e91619f7bba5e3cfb5a42d137efa3fb61',0,'2019-11-27 13:30:35'),
	('c4700bccbac930edc4d3bf94b89ee71a25fc7dcff301fea65c0f561fb5e0880324d06b8979602264','49f12d60ca97f5d1a0de7d407166dc35852ca63260c4f491b4804a26bd615408cff16f26eb2c550c',0,'2019-11-18 15:47:41'),
	('cb4cb06e27988384851b7b534993efa08f27ef722de132d8a9a009320326561895ddf199e22f4ee3','6e793f421f79963fb6bf642b917b7ee22f5be84c50f402a1ec113fb89129692f0878aceb6b7b64b1',0,'2020-01-08 15:24:57'),
	('cbccc3fd93bb3e945c5d3112dc386b5b1e7cf8b0b9ff4966ec4f6f80487d322c0fd9388bfb81f24a','0ac9f76173717bd3388d3a3d7276f06919424bf79abbeca232fe3da759baa33b7ad7152a73a2f03d',0,'2019-12-10 13:44:24'),
	('d704bfb3f525346508b6dbc1ddbe681ae4ed6932c25f245989d325525030d854d7f9200f1e1b932d','e7d9df21888e9aa3baf7c480a0f01f679a7308138c3a5ac1feb03d54054deb4543304ae2caea7934',0,'2019-11-18 10:53:41'),
	('d81d0fbbb0b0ca6caa6967aceaf688099b69bf9c527d5507ece88cc4ccf82c3276d6724798eeb3c2','37ce11011f95866d6acd8380eeb73b38e1ce3f86496c4a7b7541240bf78d87e16dbb5342e13c0b31',0,'2019-11-18 10:31:05'),
	('da6831bcc2e946127603eb2788f37593cb38dbff17d623f18882952bb307e7bad0763415938c494a','351050b2b40c693a1ee76b2be4a7103af6487093a6c7715f3af8769033ab43846a00e6d2c3c160b3',0,'2019-10-21 09:48:14'),
	('dbbfb0a7cedacef85dbde29c05742f5ac46a7abc4aeb03034be5a4cf7ac00ab5d3f7258e5b9bff9d','79850557fcec7e11b039701ca5adf67b26d21ac092b87f110ed2ac942902ae219bf4d223ce03ad0b',0,'2019-12-15 10:26:59'),
	('e131556d94dbf08a0ee6c135e3aa31936fb31745fcdbdcb462a74cf3710f00b432f84907f6e79818','7eaad727666e830be4e4eb87ce9c7820d55efdb984e952b6b16ec42ceb03329ce7f5feda6a3bcbea',0,'2019-11-27 14:02:30'),
	('e247e458e98dda1a5cc4947b1534484a326d10b84b1244527e2f5e1765062bdb4cc6ab5a340e8e49','82b8976c734eac3457ede00fa3a94ab8f9b9c29a071125a4fed6334db5b710899e901f2d29bca46c',0,'2019-12-11 12:47:54'),
	('e5b7e9f0908eae24e239fad3fcd71e6a4649716ee2f0f80b55b7276e5f8d7171cff564597af6a9f4','25f83c578ac96d850d1622b97cd3f831d2e92e2031eb9e16eec50b2af5a78d63da4d16442bc62c71',0,'2020-01-07 23:17:04'),
	('ea2e4f5604cd951eaf1dd1661a23210b1537193eb27a393a5c9dd62104bf551e1e67ed819bf377fc','8200b6d1e5778ccbb83cae726ef640ba7df81f603f359b5ed218e80409d19bed79d26c339ac0b18d',0,'2019-11-27 13:49:27'),
	('ea8da736a5c56c6afc4330ec94dc821515f204f66b9613ea476fa72e0d73f52f8e280e2ea740e79e','29cf3bcb984b867f20865e267dd7432608c69f9ee7e7b951d3f7b58c6c2f61a327b254bb8482c810',0,'2019-12-15 10:02:48'),
	('eb8db4941371b865916fa809b04599a8abb546de9b12aeb0516be292d2cf01cb3099b9067daaff9d','e8f4d1596f3fc6024b66fec272acbfffcdda2bb1f817917a3b983a1897b8a202d5a9ec1cce5655e3',0,'2020-01-08 22:50:48'),
	('ed73489d9c299bd158ad6adf5776b3a3b8f92ed195a66c20d04412e552322d3802b2bc7e86abc30a','3553d89c6b93ee57b33c6de1242afa4ef74addf6863b9241e2b0c5b2d76be855ca4a694cdc0fdaea',0,'2019-12-12 16:07:52'),
	('ef4fd9da8909f0fb550ed1661072138e39b05c51d930515463a0371eafbb746e98127ded933dddcc','fd98bfae353ed5295825475105c48c8913a8c4a1065ebfb7e34fb097a6c22e8dddd9a41582c6c8a9',0,'2019-08-28 11:18:02'),
	('f11ac0b78a71f892c381558a626996cd4d7617e0cc17bcc20967307226b0b90ba2e77d8e52bb59d4','d52a8efece0036c0d096b53254d516ecb8612cf808f25e7e7e3aed9d296fa240afef67413105817f',0,'2019-12-02 07:23:13'),
	('f20bdd8d10863b4958f7f7daff4eed3c6a790ef98cabd694a874f500aac374796be28656f276e7bb','c67b9673afe416dd56698897b9c4cf715bd7c513759b19a84665ac5d3345b2a1188b1544a720cf26',0,'2019-11-28 14:37:29'),
	('f48ff242c81eae046780f1a681de277914a9be58e8e7c1e9d3cd8521695b34687d4ab4fbaa689fe5','edf6478d111024b4b3da8305c28a56cdfbb39dd9ba99a61c1c44f04282aac2b2b2a83511807279fc',0,'2019-10-05 17:08:04'),
	('f5dd5ac01b28a312e7d60d2d3ea4fb4b4d3a1c416bb3503ce40ecfba5e386f1a2e40fc75abb1218c','b4f11a28a1352abab96d9f3466ef7e3f731df52092a182b922ebfadfc46f19ba87ee0d4baac3398f',0,'2020-01-07 23:37:06'),
	('f7eb6f3678aa17869b013498c7497adaf750f73b64519c9a690f695056e5903919a48d26b55a6192','450483275c6a5d4200841bf1a637e0193a3d1b6b5d8ee6977ec6591768597976f799642d6376b503',0,'2019-11-27 14:46:07'),
	('f9b1dcb778012d81388f1626f3921351994a9017ba0ff2f47fa396ab3be354b4dc6e7c977bcbf5f0','8c352ba6c8e9c142b559238ae36c19bdce40d08931d3029392a2df5eae025cc477a8986d0df822e2',0,'2019-08-29 14:05:40'),
	('fbab650ddcb01d4c16c87228173f8b2860112f8a312b37c0339aa1d0349b7f59cf622c1e2b02bcc2','15c83d83439c6a9f3dcf36126d4ac52d17884282a5925697fb89770cf800c4fbb348ebc0b37f2403',0,'2020-01-11 10:15:53'),
	('fbc9c92e735793b6d676939f6717a6966bfd33c70fc6b15a94761014e917e09807be9606e0cfb8af','d2021ffc821d55aaf641ad554602e6c629df4f94f4cf6a997e0b46f02084b8e3a8cf7874c31ba9e5',0,'2019-11-28 13:21:50'),
	('fd95207f6d03389fa0c76141b7e765549a27c742dc5d411e396a88ddbec03530d94b2c329e648ed7','60d85c675bc92f88a3b6085eb56a07e5fb0ebbf28f9fae1ac11e6b98f3a37d091d5f46f78930642e',0,'2020-01-08 15:33:15');

/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table questions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `questions`;

CREATE TABLE `questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `quiz_id` int(10) unsigned NOT NULL,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `choices` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` char(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `questions_quiz_id_index` (`quiz_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `questions` WRITE;
/*!40000 ALTER TABLE `questions` DISABLE KEYS */;

INSERT INTO `questions` (`id`, `quiz_id`, `question`, `choices`, `answer`, `created_at`, `updated_at`)
VALUES
	(1,1,'Question 1','{\"a\":\"choice 1\",\"b\":\"choice 2\",\"c\":\"choice 3\",\"d\":\"choice 4\"}','a','2018-08-28 18:52:18','2018-08-28 18:52:18'),
	(2,1,'Question 2','{\"a\":\"choice 1\",\"b\":\"choice 2\",\"c\":\"choice 3\",\"d\":\"choice 4\"}','b','2018-08-28 18:52:18','2018-08-28 18:52:18'),
	(3,1,'Question 3','{\"a\":\"choice 1\",\"b\":\"choice 2\",\"c\":\"choice 3\",\"d\":\"choice 4\"}','a','2018-08-28 18:52:18','2018-08-28 18:52:18'),
	(4,3,'Who is the first man on space?','{\"a\":\"question a\",\"b\":\"question b\",\"ac\":\"question c\"}','a','2018-08-29 23:24:35','2018-08-29 23:24:35'),
	(5,3,'Who is the second man on space?','{\"a\":\"question a\",\"b\":\"question b\",\"ac\":\"question c\"}','b','2018-08-29 23:24:35','2018-08-29 23:24:35'),
	(6,4,'reniel','{\"a\":\"question a\",\"b\":\"question b\",\"ac\":\"question c\"}','a','2018-09-03 22:01:46','2018-09-03 22:01:46'),
	(7,4,'reniel','{\"a\":\"question c\",\"b\":\"question b\"}','b','2018-09-03 22:01:46','2018-09-03 22:01:46'),
	(8,5,'reniel','{\"a\":\"question a\",\"b\":\"question b\",\"ac\":\"question c\"}','a','2018-09-05 00:04:51','2018-09-05 00:04:51'),
	(9,5,'reniel','{\"a\":\"question c\",\"b\":\"question b\"}','b','2018-09-05 00:04:51','2018-09-05 00:04:51'),
	(10,6,'reniel','{\"a\":\"question a\",\"b\":\"question b\",\"c\":\"question c\"}','a','2018-10-21 11:40:37','2018-10-21 11:40:37'),
	(11,6,'reniel','{\"a\":\"question a\",\"b\":\"question b\",\"c\":\"question c\"}','b','2018-10-21 11:40:37','2018-10-21 11:40:37'),
	(12,7,'reniel','{\"a\":\"question a\",\"b\":\"question b\",\"c\":\"question c\"}','a','2018-10-21 12:01:25','2018-10-21 12:01:25'),
	(13,7,'reniel','{\"a\":\"question a\",\"b\":\"question b\",\"c\":\"question c\"}','b','2018-10-21 12:01:25','2018-10-21 12:01:25'),
	(14,8,'Who is the first man on space? ','{\"a\":\"question a\",\"b\":\"question b\",\"c\":\"question c\"}','a','2018-10-21 12:05:02','2018-10-21 12:05:02'),
	(15,8,'Who is the second man on Space?','{\"a\":\"question a\",\"b\":\"question b\",\"c\":\"question c\"}','b','2018-10-21 12:05:02','2018-10-21 12:05:02'),
	(16,10,'Sample','[\"Hshd\",\"Jdie\"]','0','2018-10-21 17:08:21','2018-10-21 17:08:21'),
	(17,10,'Second','[\"Ioos\",\"Leld\"]','1','2018-10-21 17:08:21','2018-10-21 17:08:21'),
	(18,11,'Gsjx','{\"A\":\"Ahs\",\"B\":\"Ksbx\"}','B','2018-10-21 17:23:38','2018-10-21 17:23:38'),
	(19,11,'Jdiwnz','[]','A','2018-10-21 17:23:38','2018-10-21 17:23:38'),
	(20,13,'Jabzjs','{\"A\":\"Jw\",\"B\":\"Ll\"}','B','2018-10-21 17:30:12','2018-10-21 17:30:12'),
	(21,14,'First question','{\"A\":\"Hsowla\",\"B\":\"Another question\"}','B','2018-10-21 17:33:10','2018-10-21 17:33:10'),
	(22,14,'Second questio','[]','A','2018-10-21 17:33:11','2018-10-21 17:33:11'),
	(23,15,'My first question?','{\"A\":\"1 choice a\",\"B\":\"1 choice B\",\"C\":\"1 choice C\"}','B','2018-10-21 17:37:29','2018-10-21 17:37:29'),
	(24,15,'Second question','{\"A\":\"2 choice A\",\"B\":\"2 choice B\",\"C\":\"2 choice C\"}','C','2018-10-21 17:37:29','2018-10-21 17:37:29'),
	(25,16,'My first question?','{\"A\":\"1 choice a\",\"B\":\"1 choice B\",\"C\":\"1 choice C\"}','B','2018-10-21 17:37:33','2018-10-21 17:37:33'),
	(26,16,'Second question','{\"A\":\"2 choice A\",\"B\":\"2 choice B\",\"C\":\"2 choice C\"}','C','2018-10-21 17:37:33','2018-10-21 17:37:33'),
	(27,17,'First','{\"A\":\"Choice a\",\"B\":\"Choice B\"}','A','2018-10-21 17:44:05','2018-10-21 17:44:05'),
	(28,18,'First','{\"A\":\"Choice a\",\"B\":\"Choice B\"}','A','2018-10-21 17:44:10','2018-10-21 17:44:10'),
	(29,19,'Sample question 1?','{\"A\":\"Choice a\",\"B\":\"Choice b\",\"C\":\"Choice c\"}','A','2018-10-21 17:50:12','2018-10-21 17:50:12'),
	(30,19,'Second question?','{\"A\":\"Choice a\",\"B\":\"Choice b\",\"C\":\"Choice for c\"}','B','2018-10-21 17:50:13','2018-10-21 17:50:13'),
	(31,20,'New years reso','{\"A\":\"Kumain\",\"B\":\"Magpataba\"}','A','2019-01-09 21:38:24','2019-01-09 21:38:24'),
	(32,20,'Fav food?','{\"A\":\"Kare-kare\",\"B\":\"Menudo\"}','B','2019-01-09 21:38:24','2019-01-09 21:38:24');

/*!40000 ALTER TABLE `questions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table quizzes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `quizzes`;

CREATE TABLE `quizzes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lecture_id` int(10) unsigned DEFAULT NULL,
  `teacher_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_limit` int(11) NOT NULL,
  `quiz_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `quizzes_teacher_id_index` (`teacher_id`),
  KEY `quizzes_lecture_id_index` (`lecture_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `quizzes` WRITE;
/*!40000 ALTER TABLE `quizzes` DISABLE KEYS */;

INSERT INTO `quizzes` (`id`, `lecture_id`, `teacher_id`, `title`, `time_limit`, `quiz_date`, `created_at`, `updated_at`)
VALUES
	(1,7,1,'Test quiz 1',60,'2018-08-28','2018-08-28 18:52:18','2018-08-28 18:52:18'),
	(2,6,1,'sample',60,'2018-08-29','2018-08-29 23:20:03','2018-08-29 23:20:03'),
	(3,5,1,'sample',60,'2018-08-29','2018-08-29 23:24:35','2018-08-29 23:24:35'),
	(4,NULL,1,'testsss',60,'2018-09-03','2018-09-03 22:01:46','2018-09-03 22:01:46'),
	(5,NULL,1,'oopsp',20,'2018-09-04','2018-09-05 00:04:51','2018-09-05 00:04:51'),
	(6,NULL,1,'oopsp',20,'2018-10-21','2018-10-21 11:40:37','2018-10-21 11:40:37'),
	(7,NULL,1,'oopsp',20,'2018-10-21','2018-10-21 12:01:25','2018-10-21 12:01:25'),
	(8,9,1,'Quiz number 1',20,'2018-10-21','2018-10-21 12:05:02','2018-10-21 12:05:02'),
	(9,NULL,1,'Ggsjs',30,'2018-10-21','2018-10-21 17:05:00','2018-10-21 17:05:00'),
	(10,NULL,1,'Sample',30,'2018-10-21','2018-10-21 17:08:21','2018-10-21 17:08:21'),
	(11,NULL,1,'Testing',30,'2018-10-21','2018-10-21 17:23:37','2018-10-21 17:23:37'),
	(12,NULL,1,'Jsbz',30,'2018-10-21','2018-10-21 17:30:07','2018-10-21 17:30:07'),
	(13,NULL,1,'Jsbz',30,'2018-10-21','2018-10-21 17:30:12','2018-10-21 17:30:12'),
	(14,NULL,1,'This is a test',30,'2018-10-21','2018-10-21 17:33:10','2018-10-21 17:33:10'),
	(15,NULL,1,'This is another quiz',30,'2018-10-21','2018-10-21 17:37:29','2018-10-21 17:37:29'),
	(16,NULL,1,'This is another quiz',30,'2018-10-21','2018-10-21 17:37:33','2018-10-21 17:37:33'),
	(17,NULL,1,'Another',30,'2018-10-21','2018-10-21 17:44:05','2018-10-21 17:44:05'),
	(18,NULL,1,'Another',30,'2018-10-21','2018-10-21 17:44:10','2018-10-21 17:44:10'),
	(19,NULL,1,'This is a test quiz',30,'2018-10-21','2018-10-21 17:50:12','2018-10-21 17:50:12'),
	(20,NULL,1,'January 2019 001',5,'2019-01-09','2019-01-09 21:38:24','2019-01-09 21:38:24');

/*!40000 ALTER TABLE `quizzes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table recitation_answers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recitation_answers`;

CREATE TABLE `recitation_answers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `student_id` int(10) unsigned DEFAULT NULL,
  `recitation_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recitation_answers_student_id_index` (`student_id`),
  KEY `recitation_answers_recitation_id_index` (`recitation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `recitation_answers` WRITE;
/*!40000 ALTER TABLE `recitation_answers` DISABLE KEYS */;

INSERT INTO `recitation_answers` (`id`, `answer`, `student_id`, `recitation_id`, `created_at`, `updated_at`)
VALUES
	(5,'sefsefsef',2,1,'2018-12-27 18:47:54','2018-12-27 18:47:54'),
	(6,'sef',2,6,'2018-12-27 18:49:05','2018-12-27 18:49:05');

/*!40000 ALTER TABLE `recitation_answers` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table recitations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `recitations`;

CREATE TABLE `recitations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `teacher_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `recitations_teacher_id_index` (`teacher_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `recitations` WRITE;
/*!40000 ALTER TABLE `recitations` DISABLE KEYS */;

INSERT INTO `recitations` (`id`, `question`, `teacher_id`, `created_at`, `updated_at`)
VALUES
	(1,'sample question 1',1,'2018-09-07 00:39:39','2018-09-07 00:39:39'),
	(2,'sample question 2',1,'2018-09-07 00:39:42','2018-09-07 00:39:42'),
	(3,'sample question 5',1,'2018-09-07 00:52:05','2018-09-07 00:52:05'),
	(4,'Itufhd',1,'2018-10-06 01:40:51','2018-10-06 01:40:51'),
	(5,'Hhgh',1,'2018-10-06 01:47:14','2018-10-06 01:47:14'),
	(6,'OcDawgs',1,'2018-10-06 03:19:08','2018-10-06 03:19:08');

/*!40000 ALTER TABLE `recitations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` enum('student','teacher') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'student',
  `birthdate` date DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `username`, `email`, `password`, `user_type`, `birthdate`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'John Doe','john.doe','12345','$2y$10$x2TuF2Qn55ykh.Hyb6QrzuUMoDDWCjCqnCAK23dHdVBrMz5PaKCde','teacher',NULL,NULL,'2018-08-28 18:52:17','2018-08-28 18:52:17'),
	(2,'Student 1','stud1',NULL,'$2y$10$9bH.4MRVZOrhDgNfwMLkKehpDcGt4.66pHAs.WuUG4vJEc8Pi3Z1m','student',NULL,NULL,'2018-08-28 18:52:18','2018-08-28 18:52:18'),
	(3,'Student 2','stud2',NULL,'$2y$10$WlD29bWYDbh0yHxNiG15O.67SAfayvUkpugy2jHUy3EGuCqlN2ugW','student',NULL,NULL,'2018-08-28 18:52:18','2018-08-28 18:52:18'),
	(4,'Student 3',NULL,NULL,'$2y$10$Fp8Ty.QSHmQqF85weHwc5eAPaO5MQ8cUuqXV5AVWxpJqyAd0q91xa','student',NULL,NULL,'2018-08-28 18:52:18','2018-08-28 18:52:18'),
	(5,'Reniel Salvador',NULL,NULL,'$2y$10$aBVM9o.Mk5JEMCXwOpPgg.P3dx7VBmWidEgmwQomkB1SARncrizV.','student',NULL,NULL,'2018-08-29 22:20:00','2018-08-29 22:20:00'),
	(6,'Dixie Davis III',NULL,NULL,'$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','student',NULL,'LBUwRN35Kl','2018-09-16 02:24:37','2018-09-16 02:24:37'),
	(7,'Dr. Lois Heaney',NULL,NULL,'$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','student',NULL,'jPZS62XyrK','2018-09-16 02:26:54','2018-09-16 02:26:54'),
	(8,'Polly Stehr',NULL,NULL,'$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','student',NULL,'duNWlnjZvm','2018-09-16 02:27:22','2018-09-16 02:27:22'),
	(9,'Alexis Carroll',NULL,NULL,'$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm','student',NULL,'dwXQ3ChcYa','2018-09-16 02:30:13','2018-09-16 02:30:13'),
	(10,'Reniel Salvador',NULL,NULL,'$2y$10$5ullE4Wp6a/UQZ0ZDU997uMssOjm6BsO3UCHPtIK9O6qsW21kF2n.','teacher',NULL,NULL,'2018-11-17 19:25:00','2018-11-17 19:25:00'),
	(11,'Rtgf',NULL,NULL,'$2y$10$LWG95oHwHYmQhwFGb.z6wuNNFpv6IGOOQpoGzs4VaYo0Eux3Z4HTS','teacher',NULL,NULL,'2018-11-18 18:11:13','2018-11-18 18:11:13'),
	(12,'Dghv',NULL,NULL,'$2y$10$DuAj9UqtXent33.ffPHcpuxvYHfmlu6EUM3b2bz38LtHCEYahs6K6','teacher',NULL,NULL,'2018-11-18 18:22:47','2018-11-18 18:22:47'),
	(13,'Dghj',NULL,NULL,'$2y$10$VpSGJgL/XloW.Bn0.UBYXOKv.m4/YNbanvHgtj4L5e1C5l40BiNam','teacher',NULL,NULL,'2018-11-18 18:23:28','2018-11-18 18:23:28'),
	(14,'Jzjjz',NULL,NULL,'$2y$10$MI7A8snsOZMrlHIq1oAadeJCblZwItz45a6wKWElWYHIC7SR8eDCC','teacher',NULL,NULL,'2018-11-18 18:31:05','2018-11-18 18:31:05'),
	(15,'Jzhxbs',NULL,NULL,'$2y$10$U9Yj/1WaQbX/4MchxIW9auLrHYjEVtnDUNlVZxfhwlG8mFlQxkrXC','teacher',NULL,NULL,'2018-11-18 18:37:02','2018-11-18 18:37:02'),
	(16,'Jshd',NULL,NULL,'$2y$10$crNYQwCMS/Gxu7CUmi2Di.H5mRAasMw7DhASSQrEJJ/Q5RyIFUZXa','teacher',NULL,NULL,'2018-11-18 18:43:14','2018-11-18 18:43:14'),
	(17,'Jsjdbs',NULL,NULL,'$2y$10$dk6yhfyDjtgs5Gzz7OZu.udz34.uAmh5feOA6iLWWoPkSD/LUf5v2','teacher',NULL,NULL,'2018-11-18 18:53:40','2018-11-18 18:53:40'),
	(18,'Kkkk',NULL,NULL,'$2y$10$hc0mm0la8htIjywzsv1WbO8dLJFKQI.7jluy4f6HjnXtHyrPjcG3C','teacher',NULL,NULL,'2018-11-18 23:08:00','2018-11-18 23:08:00'),
	(19,'Ttt',NULL,NULL,'$2y$10$PP5k848UjEvUsniH0xbDeeTvqvZEe4yQLxMR8cVaZCyyEw1DIH.gC','teacher',NULL,NULL,'2018-11-18 23:48:27','2018-11-18 23:48:27'),
	(20,'Done',NULL,NULL,'$2y$10$GerW2NRnMHju6.1c0t79y.3DyfdecQZk5m/x3kPANoWH34Usx2/R6','teacher',NULL,NULL,'2018-12-02 16:26:01','2018-12-02 16:26:01'),
	(21,'Student3',NULL,NULL,'$2y$10$4PCdMd60YYphmsnAJl8az.hsa/yExdJl.4nRUhlqQ5vCyf3RhW1Lm','student',NULL,NULL,'2018-12-02 16:27:35','2018-12-02 16:27:35'),
	(22,'Hsjabs',NULL,NULL,'$2y$10$vOxs1culHuZjHTbjg1KrEuyqs7LRBFNyiGy3TYvVsuGYdookVWMoa','student',NULL,NULL,'2018-12-02 16:29:42','2018-12-02 16:29:42'),
	(23,'Reniel Salvador',NULL,NULL,'$2y$10$.fPrWR0vk7joENby8/h4ouHrq/HFvEJ3tBbuJBJlpsDyIB/rMLrES','student',NULL,NULL,'2019-01-08 15:13:44','2019-01-08 15:13:44'),
	(25,'Reniel Salvador','2222',NULL,'$2y$10$Oo1/yoFJtj5e2o05ZQD0o.H0853LdQ64txVO.BKFrJXT2Nn1IayuO','student',NULL,NULL,'2019-01-08 15:24:05','2019-01-08 15:24:05'),
	(26,'John Wick','3333',NULL,'$2y$10$HWIS1FCbiW2z7UYxNfIQpucIO60LusM.sBJHYbPIFgdlq7mU/pv36','student',NULL,NULL,'2019-01-09 21:24:57','2019-01-09 21:24:57');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
